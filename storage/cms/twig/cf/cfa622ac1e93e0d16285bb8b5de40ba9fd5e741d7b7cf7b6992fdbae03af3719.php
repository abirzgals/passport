<?php

/* /var/www/html/passport/themes/passport/pages/index.htm */
class __TwigTemplate_b6452ffa9824f4f395ad7da58cc8941e7d746e98a0427b532f5f5a77dedfa30c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section>
        <div class=\"swiper-container swiper-slider swiper-variant-1\" data-loop=\"false\" data-autoplay=\"false\" data-simulate-touch=\"true\">
          <div class=\"swiper-wrapper\">
            <div class=\"swiper-slide\" data-slide-bg=\"";
        // line 4
        echo $this->extensions['System\Twig\Extension']->mediaFilter("home-slider-1-slide-1.jpg");
        echo "\">
              <div class=\"swiper-slide-caption\">
                <div class=\"container\">
                  <div class=\"row\">
                    <div class=\"col-md-8 col-lg-7\">
                      <h2 class=\"slider-header\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"0s\"><span class=\"sub-head\">Pass boundary lines freely and secure or Pass freely to your secure life</span></h2>
                      <p class=\"text-big slider-text text-black\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"100\">We can insure you from any kind of trouble</p>
                      <a class=\"btn btn-icon btn-icon-right btn-cello btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"#contact-form\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>Get a Quote</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class=\"section-50 section-lg-75 section-lg-90 section-xl-100\">
        <div class=\"container\">
          <div class=\"row align-items-lg-center\">
            <div class=\"col-lg-6\">
              <div class=\"video-wrap\">
                <!--<figure class=\"overlay-5\"><img class=\"img-responsive\" src=\"";
        // line 26
        echo $this->extensions['System\Twig\Extension']->mediaFilter("home-04-571x378.jpg");
        echo "\" width=\"571\" height=\"378\" alt=\"\"></figure><a class=\"link-circle link-white\" data-lightbox=\"iframe\" href=\"https://www.youtube.com/watch?v=1UWpbtUupQQ\"><span class=\"icon icon-xl fl-36-slim-icons-play90\"></span></a>-->
                <img src=\"";
        // line 27
        echo $this->extensions['System\Twig\Extension']->mediaFilter("blog-classic-5-970x546.jpg");
        echo "\">
                
                
              </div>
            </div>
            <div class=\"col-lg-6\">
              <div class=\"inset-xl-left-70 inset-xl-right-70\">
                <h2><span class=\"sub-head-small\">About Us</span></h2>
                <p class=\"text-cello\">At FlagPass our aim is straightforward: to help our clients enjoy a better lifestyle today while planning for a secure future tomorrow.
We strongly believe in traditional values and virtues such as reliability, respect and integrity, meeting the diverse needs of our clients by offering bespoke, personal advice.</p>
                <div class=\"row text-cello\">
                  <div class=\"col-md-5\">
                    <ul class=\"list-marked-dotted\">
                      <li>Experienced team;</li>
                      <li>Smooth and stress-free process;</li>
                    </ul>
                  </div>
                  <div class=\"col-md-7\">
                    <ul class=\"list-marked-dotted\">
                      <li>Many years of experience;</li>
                      <li>Processing is fast and efficient.</li>
                    </ul>
                  </div>
                </div>
                <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about-us");
        echo "\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>Read More</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      
      
      
      
      <div id=\"citizenship\" style=\"position:relative;top:-50px;\"></div>


      <section class=\"bg-iron bg-iron-custom section-60 section-md-90 section-xl-bottom-15 bg-default\" style=\"padding:45px 15px 0px 15px\">
        <div class=\"container\">
            <div style=\"text-align:center;\">
            <h2><span class=\"sub-head-small\">Most popular products</span></h2>
            </div>
            <h3 class=\"text-spacing-25\">Citizenship</h3>
        </div>
      </section>

      <section class=\"bg-iron bg-iron-custom bg-whisper-1 bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-100 section-xl-bottom-100 bg-default\" style=\"padding:30px 15px 0px 15px\">
        <div class=\"container bg-decoration-content\">
          <div class=\"row justify-content-md-center\">
            <div class=\"col-lg-10 col-xl-12\">
              <div class=\"row align-items-md-end row-40\">
              
              
              ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["citizenship_items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 82
            echo "               <div class=\"col-md-6 col-xl-4\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\"><a href=\"";
            // line 85
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">";
            // line 86
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", array()), 0, ","), "html", null, true);
            echo "</span><span class=\"small small-bottom\"></span></div>
                      <ul class=\"pricing-list\">
                        ";
            // line 88
            echo twig_get_attribute($this->env, $this->source, $context["item"], "benefits", array());
            echo "
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-icon btn-icon-right btn-primary-contrast btn-custom btn-block\" href=\"";
            // line 91
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">Sign up</a></div>
                  </div>
                </div>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "              <!--
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">free</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">0</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Life Insurance</li>
                        <li>Basic Coverage</li>
                        <li>Home Insurance</li>
                        <li>Personal Account</li>
                        <li>Phone support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Starter</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">59</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Car Insurance</li>
                        <li>Increased Coverage</li>
                        <li>Travel Insurance</li>
                        <li>Free Mobile App</li>
                        <li>Online Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table pricing-table-preferred\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Business</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">99</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Business Insurance</li>
                        <li>Advanced Coverage</li>
                        <li>Law Insurance</li>
                        <li>Free Insurance Report</li>
                        <li>24/7 Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Ultimate</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">199</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Umbrella Insurance</li>
                        <li>Full Coverage</li>
                        <li>Flood Insurance</li>
                        <li>Personal Assistant</li>
                        <li>Premium Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                -->
                
              </div>
            </div>
          </div>
          <div class=\"row justify-content-md-center\">
            <div class=\"col-md-4 col-lg-3\" style=\"text-align: center;\">
                 <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"";
        // line 174
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenships");
        echo "\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>More products</a></div>   
            </div>
          </div>
        </div>
        <div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
      </section>


 <div id=\"second-passport\" style=\"position:relative;top:-50px;\"></div>
      <section class=\"bg-iron bg-iron-custom section-60 section-md-90 section-xl-bottom-15 bg-default\" style=\"padding:50px 15px 0px 15px\">
        <div class=\"container\">
            <div style=\"border:1px solid #000;\"></div>
            <h3 class=\"text-spacing--25\">Second passport</h3>
        </div>
      </section>


      <section class=\"bg-iron bg-iron-custom bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-100 section-xl-bottom-100 bg-default\" style=\"padding:30px 15px 50px 15px\">
        <div class=\"container bg-decoration-content\">
          <div class=\"row justify-content-md-center\">
            <div class=\"col-lg-10 col-xl-12\">
              <div class=\"row align-items-md-end row-40\">
              
              
              ";
        // line 198
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["second_passport_items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 199
            echo "               <div class=\"col-md-6 col-xl-4\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\"><a href=\"";
            // line 202
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passport", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">";
            // line 203
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", array()), 0, ","), "html", null, true);
            echo "</span><span class=\"small small-bottom\"></span></div>
                      <ul class=\"pricing-list\">
                        ";
            // line 205
            echo twig_get_attribute($this->env, $this->source, $context["item"], "benefits", array());
            echo "
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-icon btn-icon-right btn-primary-contrast btn-custom btn-block\" href=\"";
            // line 208
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passport", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">Sign up</a></div>
                  </div>
                </div>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 212
        echo "              
              <!--
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">free</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">0</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Life Insurance</li>
                        <li>Basic Coverage</li>
                        <li>Home Insurance</li>
                        <li>Personal Account</li>
                        <li>Phone support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Starter</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">59</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Car Insurance</li>
                        <li>Increased Coverage</li>
                        <li>Travel Insurance</li>
                        <li>Free Mobile App</li>
                        <li>Online Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table pricing-table-preferred\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Business</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">99</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Business Insurance</li>
                        <li>Advanced Coverage</li>
                        <li>Law Insurance</li>
                        <li>Free Insurance Report</li>
                        <li>24/7 Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Ultimate</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">199</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Umbrella Insurance</li>
                        <li>Full Coverage</li>
                        <li>Flood Insurance</li>
                        <li>Personal Assistant</li>
                        <li>Premium Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                -->
              </div>
            </div>
          </div>
          <div class=\"row justify-content-md-center\">
            <div class=\"col-md-4 col-lg-3\" style=\"text-align: center;\">
            
                <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"";
        // line 290
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passports");
        echo "\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>More products</a></div>            
            

            </div>
          </div>          
        </div>
        <div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
      </section>

      
      
      
      
      
      ";
        // line 304
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("contact-form"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 304,  377 => 290,  297 => 212,  287 => 208,  281 => 205,  276 => 203,  270 => 202,  265 => 199,  261 => 198,  234 => 174,  153 => 95,  143 => 91,  137 => 88,  132 => 86,  126 => 85,  121 => 82,  117 => 81,  84 => 51,  57 => 27,  53 => 26,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section>
        <div class=\"swiper-container swiper-slider swiper-variant-1\" data-loop=\"false\" data-autoplay=\"false\" data-simulate-touch=\"true\">
          <div class=\"swiper-wrapper\">
            <div class=\"swiper-slide\" data-slide-bg=\"{{ 'home-slider-1-slide-1.jpg'|media }}\">
              <div class=\"swiper-slide-caption\">
                <div class=\"container\">
                  <div class=\"row\">
                    <div class=\"col-md-8 col-lg-7\">
                      <h2 class=\"slider-header\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"0s\"><span class=\"sub-head\">Pass boundary lines freely and secure or Pass freely to your secure life</span></h2>
                      <p class=\"text-big slider-text text-black\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"100\">We can insure you from any kind of trouble</p>
                      <a class=\"btn btn-icon btn-icon-right btn-cello btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"#contact-form\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>Get a Quote</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class=\"section-50 section-lg-75 section-lg-90 section-xl-100\">
        <div class=\"container\">
          <div class=\"row align-items-lg-center\">
            <div class=\"col-lg-6\">
              <div class=\"video-wrap\">
                <!--<figure class=\"overlay-5\"><img class=\"img-responsive\" src=\"{{ 'home-04-571x378.jpg'|media}}\" width=\"571\" height=\"378\" alt=\"\"></figure><a class=\"link-circle link-white\" data-lightbox=\"iframe\" href=\"https://www.youtube.com/watch?v=1UWpbtUupQQ\"><span class=\"icon icon-xl fl-36-slim-icons-play90\"></span></a>-->
                <img src=\"{{ 'blog-classic-5-970x546.jpg'|media }}\">
                
                
              </div>
            </div>
            <div class=\"col-lg-6\">
              <div class=\"inset-xl-left-70 inset-xl-right-70\">
                <h2><span class=\"sub-head-small\">About Us</span></h2>
                <p class=\"text-cello\">At FlagPass our aim is straightforward: to help our clients enjoy a better lifestyle today while planning for a secure future tomorrow.
We strongly believe in traditional values and virtues such as reliability, respect and integrity, meeting the diverse needs of our clients by offering bespoke, personal advice.</p>
                <div class=\"row text-cello\">
                  <div class=\"col-md-5\">
                    <ul class=\"list-marked-dotted\">
                      <li>Experienced team;</li>
                      <li>Smooth and stress-free process;</li>
                    </ul>
                  </div>
                  <div class=\"col-md-7\">
                    <ul class=\"list-marked-dotted\">
                      <li>Many years of experience;</li>
                      <li>Processing is fast and efficient.</li>
                    </ul>
                  </div>
                </div>
                <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"{{'about-us'|page}}\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>Read More</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      
      
      
      
      <div id=\"citizenship\" style=\"position:relative;top:-50px;\"></div>


      <section class=\"bg-iron bg-iron-custom section-60 section-md-90 section-xl-bottom-15 bg-default\" style=\"padding:45px 15px 0px 15px\">
        <div class=\"container\">
            <div style=\"text-align:center;\">
            <h2><span class=\"sub-head-small\">Most popular products</span></h2>
            </div>
            <h3 class=\"text-spacing-25\">Citizenship</h3>
        </div>
      </section>

      <section class=\"bg-iron bg-iron-custom bg-whisper-1 bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-100 section-xl-bottom-100 bg-default\" style=\"padding:30px 15px 0px 15px\">
        <div class=\"container bg-decoration-content\">
          <div class=\"row justify-content-md-center\">
            <div class=\"col-lg-10 col-xl-12\">
              <div class=\"row align-items-md-end row-40\">
              
              
              {%for item in citizenship_items%}
               <div class=\"col-md-6 col-xl-4\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\"><a href=\"{{'citizenship'|page({'slug':item.slug})}}\">{{item.name}}</a></h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">{{item.price|number_format(0, ',') }}</span><span class=\"small small-bottom\"></span></div>
                      <ul class=\"pricing-list\">
                        {{item.benefits|raw}}
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-icon btn-icon-right btn-primary-contrast btn-custom btn-block\" href=\"{{'citizenship'|page({'slug':item.slug})}}\">Sign up</a></div>
                  </div>
                </div>
              {%endfor%}
              <!--
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">free</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">0</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Life Insurance</li>
                        <li>Basic Coverage</li>
                        <li>Home Insurance</li>
                        <li>Personal Account</li>
                        <li>Phone support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Starter</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">59</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Car Insurance</li>
                        <li>Increased Coverage</li>
                        <li>Travel Insurance</li>
                        <li>Free Mobile App</li>
                        <li>Online Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table pricing-table-preferred\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Business</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">99</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Business Insurance</li>
                        <li>Advanced Coverage</li>
                        <li>Law Insurance</li>
                        <li>Free Insurance Report</li>
                        <li>24/7 Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Ultimate</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">199</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Umbrella Insurance</li>
                        <li>Full Coverage</li>
                        <li>Flood Insurance</li>
                        <li>Personal Assistant</li>
                        <li>Premium Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                -->
                
              </div>
            </div>
          </div>
          <div class=\"row justify-content-md-center\">
            <div class=\"col-md-4 col-lg-3\" style=\"text-align: center;\">
                 <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"{{'citizenships'|page}}\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>More products</a></div>   
            </div>
          </div>
        </div>
        <div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
      </section>


 <div id=\"second-passport\" style=\"position:relative;top:-50px;\"></div>
      <section class=\"bg-iron bg-iron-custom section-60 section-md-90 section-xl-bottom-15 bg-default\" style=\"padding:50px 15px 0px 15px\">
        <div class=\"container\">
            <div style=\"border:1px solid #000;\"></div>
            <h3 class=\"text-spacing--25\">Second passport</h3>
        </div>
      </section>


      <section class=\"bg-iron bg-iron-custom bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-100 section-xl-bottom-100 bg-default\" style=\"padding:30px 15px 50px 15px\">
        <div class=\"container bg-decoration-content\">
          <div class=\"row justify-content-md-center\">
            <div class=\"col-lg-10 col-xl-12\">
              <div class=\"row align-items-md-end row-40\">
              
              
              {%for item in second_passport_items%}
               <div class=\"col-md-6 col-xl-4\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\"><a href=\"{{'second-passport'|page({'slug':item.slug})}}\">{{item.name}}</a></h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">{{item.price|number_format(0, ',') }}</span><span class=\"small small-bottom\"></span></div>
                      <ul class=\"pricing-list\">
                        {{item.benefits|raw}}
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-icon btn-icon-right btn-primary-contrast btn-custom btn-block\" href=\"{{'second-passport'|page({'slug':item.slug})}}\">Sign up</a></div>
                  </div>
                </div>
              {%endfor%}
              
              <!--
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">free</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">0</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Life Insurance</li>
                        <li>Basic Coverage</li>
                        <li>Home Insurance</li>
                        <li>Personal Account</li>
                        <li>Phone support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Starter</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">59</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Car Insurance</li>
                        <li>Increased Coverage</li>
                        <li>Travel Insurance</li>
                        <li>Free Mobile App</li>
                        <li>Online Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table pricing-table-preferred\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Business</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">99</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Business Insurance</li>
                        <li>Advanced Coverage</li>
                        <li>Law Insurance</li>
                        <li>Free Insurance Report</li>
                        <li>24/7 Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                <div class=\"col-md-6 col-xl-3\">
                  <div class=\"pricing-table\">
                    <div class=\"pricing-table-body\">
                      <h5 class=\"pricing-table-header\">Ultimate</h5>
                      <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">199</span><span class=\"small small-bottom\">/mo</span></div>
                      <div class=\"divider-circle\"></div>
                      <ul class=\"pricing-list\">
                        <li>Umbrella Insurance</li>
                        <li>Full Coverage</li>
                        <li>Flood Insurance</li>
                        <li>Personal Assistant</li>
                        <li>Premium Support</li>
                      </ul>
                    </div>
                    <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
                  </div>
                </div>
                
                -->
              </div>
            </div>
          </div>
          <div class=\"row justify-content-md-center\">
            <div class=\"col-md-4 col-lg-3\" style=\"text-align: center;\">
            
                <div class=\"button-block\"><a class=\"btn btn-icon btn-icon-right btn-cello-outline btn-custom\" data-caption-animate=\"fadeInUp\" data-caption-delay=\"250\" href=\"{{'second-passports'|page}}\"><span class=\"icon icon-xs-smaller fa-angle-right\"></span>More products</a></div>            
            

            </div>
          </div>          
        </div>
        <div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
      </section>

      
      
      
      
      
      {% partial 'contact-form' %}", "/var/www/html/passport/themes/passport/pages/index.htm", "");
    }
}
