<?php

/* /var/www/html/passport/themes/passport/partials/footer-selection.htm */
class __TwigTemplate_d5cec4bef86e091c7a9866f3ba4874d5e7ebba7f4fa7f4de31f401b15a32fcb4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-40 section-md-40 bg-cello footer-section\">
        <div class=\"container\">
          <div class=\"row justify-content-sm-center\">
            <div class=\"col-sm-9 col-md-11 col-xl-12\">
              <div class=\"row row-50\">
              
              <!--
                <div class=\"col-md-6 col-lg-10 col-xl-3\">
                  <div class=\"inset-xl-right-20\" style=\"max-width: 510px;\"><a class=\"brand brand-inverse\" href=\"index.html\"><img src=\"";
        // line 9
        echo $this->extensions['System\Twig\Extension']->mediaFilter("brand-footer.png");
        echo "\" width=\"118\" height=\"34\" alt=\"\"></a>
                    <p class=\"text-spacing--30 text-kashmir-blue\">Shelter has been a trusted name in insurance for more than 15 years. Today, we proudly serve more than 16 million customers nationwide.</p><a class=\"link link-group link-group-animated link-black link-white\" href=\"#\"><span>Free Consultation</span><span class=\"icon icon-xxs icon-primary fa fa-angle-right\"></span></a>
                  </div>
                </div>
                -->
                
                
               <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Contact us</h6>
                  <address class=\"contact-info text-left\">
                    <div class=\"unit unit-horizontal unit-spacing-xs align-items-center\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary material-icons-phone\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    </div>
                    <div class=\"unit unit-horizontal unit-spacing-xs align-items-center\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary fa fa-envelope-o\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                    </div>
                    <div class=\"unit unit-horizontal unit-spacing-xs\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary material-icons-place\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white d-inline\" href=\"#\">6036 Richmond hwy,<br>Alexandria, VA USA 22303</a></div>
                    </div>
                  </address>
                </div>
                
                <div class=\"col-md-6 col-lg-4 col-xl-4\">
                  <h6 class=\"h6 footer-title\">Services</h6>
                  <div class=\"row\" style=\"max-width: 370px;\">
                    <div class=\"col-6\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"";
        // line 39
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 40
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about-us");
        echo "\">About us</a></li>
                        <li><a href=\"";
        // line 41
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact-us");
        echo "\">Contacts</a></li>
                      </ul>
                    </div>
                    <div class=\"col-6\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"";
        // line 46
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenships");
        echo "\">Citizenship</a></li>
                        <li><a href=\"";
        // line 47
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passports");
        echo "\">Second passport</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                
                <!--
                <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Useful links</h6>
                  <div class=\"row\" style=\"max-width: 270px;\">
                    <div class=\"col-12\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"index.html\">Privacy Policy</a></li>
                        <li><a href=\"services.html\">Cookie Policy</a></li>
                        <li><a href=\"gallery-item.html\">Terms of use</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                -->
                
                <!--
                <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Recent Posts</h6>
                        <article class=\"post post-preview post-preview-inverse offset-top-22\"><a href=\"blog-post.html\">
                            <div class=\"unit unit-horizontal unit-spacing-lg\">
                              <div class=\"unit-left\">
                                <figure class=\"post-image\"><img src=\"";
        // line 75
        echo $this->extensions['System\Twig\Extension']->mediaFilter("post-preview-7-70x70.jpg");
        echo "\" alt=\"\" width=\"70\" height=\"70\"/>
                                </figure>
                              </div>
                              <div class=\"unit-body\">
                                <div class=\"post-header\">
                                  <p class=\"inset-lg-right-10\">A Look into Nonprofit Risk Management</p>
                                </div>
                                <div class=\"post-meta\">
                                  <ul class=\"list-meta\">
                                    <li>
                                      <time datetime=\"2018-02-04\">June 23, 2018 </time>
                                    </li>
                                    <li>1 Comment</li>
                                  </ul>
                                </div>
                              </div>
                            </div></a></article>
                        <article class=\"post post-preview post-preview-inverse offset-top-22\"><a href=\"blog-post.html\">
                            <div class=\"unit unit-horizontal unit-spacing-lg\">
                              <div class=\"unit-left\">
                                <figure class=\"post-image\"><img src=\"";
        // line 95
        echo $this->extensions['System\Twig\Extension']->mediaFilter("post-preview-8-70x70.jpg");
        echo "\" alt=\"\" width=\"70\" height=\"70\"/>
                                </figure>
                              </div>
                              <div class=\"unit-body\">
                                <div class=\"post-header\">
                                  <p class=\"inset-lg-right-10\">Workers’ Compensation: Combinability of Insureds</p>
                                </div>
                                <div class=\"post-meta\">
                                  <ul class=\"list-meta\">
                                    <li>
                                      <time datetime=\"2018-02-04\">June 20, 2018</time>
                                    </li>
                                    <li>1 Comment</li>
                                  </ul>
                                </div>
                              </div>
                            </div></a></article>
                </div>
                -->
                
                

                
 
                
                
                
              </div>
            </div>
          </div>
        </div>
      </section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/partials/footer-selection.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 95,  117 => 75,  86 => 47,  82 => 46,  74 => 41,  70 => 40,  66 => 39,  33 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-40 section-md-40 bg-cello footer-section\">
        <div class=\"container\">
          <div class=\"row justify-content-sm-center\">
            <div class=\"col-sm-9 col-md-11 col-xl-12\">
              <div class=\"row row-50\">
              
              <!--
                <div class=\"col-md-6 col-lg-10 col-xl-3\">
                  <div class=\"inset-xl-right-20\" style=\"max-width: 510px;\"><a class=\"brand brand-inverse\" href=\"index.html\"><img src=\"{{ 'brand-footer.png'|media}}\" width=\"118\" height=\"34\" alt=\"\"></a>
                    <p class=\"text-spacing--30 text-kashmir-blue\">Shelter has been a trusted name in insurance for more than 15 years. Today, we proudly serve more than 16 million customers nationwide.</p><a class=\"link link-group link-group-animated link-black link-white\" href=\"#\"><span>Free Consultation</span><span class=\"icon icon-xxs icon-primary fa fa-angle-right\"></span></a>
                  </div>
                </div>
                -->
                
                
               <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Contact us</h6>
                  <address class=\"contact-info text-left\">
                    <div class=\"unit unit-horizontal unit-spacing-xs align-items-center\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary material-icons-phone\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    </div>
                    <div class=\"unit unit-horizontal unit-spacing-xs align-items-center\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary fa fa-envelope-o\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                    </div>
                    <div class=\"unit unit-horizontal unit-spacing-xs\">
                      <div class=\"unit-left\"><span class=\"icon icon-xs icon-primary material-icons-place\"></span></div>
                      <div class=\"unit-body\"><a class=\"link-white d-inline\" href=\"#\">6036 Richmond hwy,<br>Alexandria, VA USA 22303</a></div>
                    </div>
                  </address>
                </div>
                
                <div class=\"col-md-6 col-lg-4 col-xl-4\">
                  <h6 class=\"h6 footer-title\">Services</h6>
                  <div class=\"row\" style=\"max-width: 370px;\">
                    <div class=\"col-6\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"{{ 'index'|page }}\">Home</a></li>
                        <li><a href=\"{{ 'about-us'|page }}\">About us</a></li>
                        <li><a href=\"{{ 'contact-us'|page }}\">Contacts</a></li>
                      </ul>
                    </div>
                    <div class=\"col-6\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"{{ 'citizenships'|page }}\">Citizenship</a></li>
                        <li><a href=\"{{ 'second-passports'|page }}\">Second passport</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                
                <!--
                <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Useful links</h6>
                  <div class=\"row\" style=\"max-width: 270px;\">
                    <div class=\"col-12\">
                      <ul class=\"list-marked-variant-2\">
                        <li><a href=\"index.html\">Privacy Policy</a></li>
                        <li><a href=\"services.html\">Cookie Policy</a></li>
                        <li><a href=\"gallery-item.html\">Terms of use</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                -->
                
                <!--
                <div class=\"col-md-6 col-lg-4 col-xl-3\">
                  <h6 class=\"h6 footer-title\">Recent Posts</h6>
                        <article class=\"post post-preview post-preview-inverse offset-top-22\"><a href=\"blog-post.html\">
                            <div class=\"unit unit-horizontal unit-spacing-lg\">
                              <div class=\"unit-left\">
                                <figure class=\"post-image\"><img src=\"{{ 'post-preview-7-70x70.jpg'|media}}\" alt=\"\" width=\"70\" height=\"70\"/>
                                </figure>
                              </div>
                              <div class=\"unit-body\">
                                <div class=\"post-header\">
                                  <p class=\"inset-lg-right-10\">A Look into Nonprofit Risk Management</p>
                                </div>
                                <div class=\"post-meta\">
                                  <ul class=\"list-meta\">
                                    <li>
                                      <time datetime=\"2018-02-04\">June 23, 2018 </time>
                                    </li>
                                    <li>1 Comment</li>
                                  </ul>
                                </div>
                              </div>
                            </div></a></article>
                        <article class=\"post post-preview post-preview-inverse offset-top-22\"><a href=\"blog-post.html\">
                            <div class=\"unit unit-horizontal unit-spacing-lg\">
                              <div class=\"unit-left\">
                                <figure class=\"post-image\"><img src=\"{{ 'post-preview-8-70x70.jpg'|media}}\" alt=\"\" width=\"70\" height=\"70\"/>
                                </figure>
                              </div>
                              <div class=\"unit-body\">
                                <div class=\"post-header\">
                                  <p class=\"inset-lg-right-10\">Workers’ Compensation: Combinability of Insureds</p>
                                </div>
                                <div class=\"post-meta\">
                                  <ul class=\"list-meta\">
                                    <li>
                                      <time datetime=\"2018-02-04\">June 20, 2018</time>
                                    </li>
                                    <li>1 Comment</li>
                                  </ul>
                                </div>
                              </div>
                            </div></a></article>
                </div>
                -->
                
                

                
 
                
                
                
              </div>
            </div>
          </div>
        </div>
      </section>", "/var/www/html/passport/themes/passport/partials/footer-selection.htm", "");
    }
}
