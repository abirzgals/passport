<?php

/* /var/www/html/passport/themes/passport/partials/ajax-response.htm */
class __TwigTemplate_9bb0e22e231d330be944facf773b65ad56bcd156946de76d2fc9cadedf19b8c2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, ($context["result"] ?? null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/partials/ajax-response.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{result}}", "/var/www/html/passport/themes/passport/partials/ajax-response.htm", "");
    }
}
