<?php

/* /var/www/html/passport/themes/passport/pages/citizenships.htm */
class __TwigTemplate_3e56efa92df1b018aa935192ea5768b1aa57cb214f97b3b3a3c30104fa42c6d7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url(";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-4.jpg");
        echo ");\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>Citizenship</h2>
      </div>
    </div>
</section>


";
        // line 10
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["breadscrumbs"] ?? null) : null)) {
            // line 11
            echo "<section class=\"bg-default bg-iron bg-iron-custom\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["breadscrumbs"] ?? null) : null));
            foreach ($context['_seq'] as $context["item"] => $context["url"]) {
                // line 18
                echo "                    <li>
                        ";
                // line 19
                if (($context["url"] != "")) {
                    // line 20
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $context["url"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 22
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "
                        ";
                }
                // line 24
                echo "                    </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['item'], $context['url'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
";
        }
        // line 33
        echo "


<section class=\"bg-iron bg-iron-custom bg-whisper-1 bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-50 section-xl-bottom-100 bg-default\" >
<div class=\"container bg-decoration-content\">
  <div class=\"row justify-content-md-center\">
  
    <div class=\"col-lg-10 col-xl-12\" style=\"margin-bottom:50px;\">
    <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    </div>
    
    <div class=\"col-lg-10 col-xl-12\">
      <div class=\"row align-items-md-end row-40\">
      
      
      ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["citizenship_items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 51
            echo "       <div class=\"col-md-6 col-xl-4\">
          <div class=\"pricing-table\">
            <div class=\"pricing-table-body\">
              <h5 class=\"pricing-table-header\"><a href=\"";
            // line 54
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></h5>
              <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">";
            // line 55
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", array()), 0, ","), "html", null, true);
            echo "</span><span class=\"small small-bottom\"></span></div>
              <ul class=\"pricing-list\">
                ";
            // line 57
            echo twig_get_attribute($this->env, $this->source, $context["item"], "benefits", array());
            echo "
              </ul>
            </div>
            <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"";
            // line 60
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">Sign up</a></div>
          </div>
        </div>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "        
      </div>
    </div>
  </div>
</div>
<div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/citizenships.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 64,  130 => 60,  124 => 57,  119 => 55,  113 => 54,  108 => 51,  104 => 50,  85 => 33,  76 => 26,  69 => 24,  63 => 22,  55 => 20,  53 => 19,  50 => 18,  46 => 17,  38 => 11,  36 => 10,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url({{ 'bg-image-4.jpg'|media }});\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>Citizenship</h2>
      </div>
    </div>
</section>


{% if this.page[\"breadscrumbs\"] %}
<section class=\"bg-default bg-iron bg-iron-custom\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    {%for item,url in this.page[\"breadscrumbs\"]%}
                    <li>
                        {% if url!=\"\" %}
                            <a href=\"{{url}}\">{{item}}</a>
                        {% else %}
                            {{item}}
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
{% endif %}



<section class=\"bg-iron bg-iron-custom bg-whisper-1 bg-decoration-wrap bg-decoration-bottom section-bottom-60 section-xl-top-50 section-xl-bottom-100 bg-default\" >
<div class=\"container bg-decoration-content\">
  <div class=\"row justify-content-md-center\">
  
    <div class=\"col-lg-10 col-xl-12\" style=\"margin-bottom:50px;\">
    <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    </div>
    
    <div class=\"col-lg-10 col-xl-12\">
      <div class=\"row align-items-md-end row-40\">
      
      
      {%for item in citizenship_items%}
       <div class=\"col-md-6 col-xl-4\">
          <div class=\"pricing-table\">
            <div class=\"pricing-table-body\">
              <h5 class=\"pricing-table-header\"><a href=\"{{'citizenship'|page({'slug':item.slug})}}\">{{item.name}}</a></h5>
              <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\">{{item.price|number_format(0, ',') }}</span><span class=\"small small-bottom\"></span></div>
              <ul class=\"pricing-list\">
                {{item.benefits|raw}}
              </ul>
            </div>
            <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"{{'citizenship'|page({'slug':item.slug})}}\">Sign up</a></div>
          </div>
        </div>
      {%endfor%}
        
      </div>
    </div>
  </div>
</div>
<div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>
</section>", "/var/www/html/passport/themes/passport/pages/citizenships.htm", "");
    }
}
