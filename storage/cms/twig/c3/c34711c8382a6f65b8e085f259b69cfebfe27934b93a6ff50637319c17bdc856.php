<?php

/* /var/www/html/passport/themes/passport/pages/contact-us.htm */
class __TwigTemplate_b0ef5d6bb29c13b17d6c4fccb135aee7142b76baef9457c37bf1520e3c12b6c3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url(";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-8.jpg");
        echo ");\">
        <div class=\"container\">
          <div class=\"page-title text-center\">
            <h2>Contact Us</h2>
          </div>
        </div>
</section>

";
        // line 9
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["breadscrumbs"] ?? null) : null)) {
            // line 10
            echo "<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["breadscrumbs"] ?? null) : null));
            foreach ($context['_seq'] as $context["item"] => $context["url"]) {
                // line 17
                echo "                    <li>
                        ";
                // line 18
                if (($context["url"] != "")) {
                    // line 19
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $context["url"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 21
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "
                        ";
                }
                // line 23
                echo "                    </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['item'], $context['url'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
";
        }
        // line 32
        echo "
      <section class=\"section-60 section-md-top-90 section-md-bottom-100\">
        <div class=\"container\">
          <div class=\"row justify-content-lg-between row-60\">
            <div class=\"col-lg-7 col-xl-6\">
              <h3 class=\"text-spacing--25\">Get in Touch</h3>
              <form class=\"rd-mailform form-modern\" id=\"contact-us-form\" data-form-output=\"form-output-global\" data-form-type=\"contact\" method=\"post\" action=\"bat/rd-mailform.php\">
                <div class=\"row row-30\">
                  <div class=\"col-md-6\">
                    <div class=\"form-wrap\">
                      <input class=\"form-input\" id=\"contact-name\" type=\"text\" name=\"name\" data-constraints=\"@Required\">
                      <label class=\"form-label\" for=\"contact-name\">Name</label>
                    </div>
                  </div>
                  <div class=\"col-md-6\">
                    <div class=\"form-wrap\">
                      <input class=\"form-input\" id=\"contact-email\" type=\"email\" name=\"email\" data-constraints=\"@Email @Required\">
                      <label class=\"form-label\" for=\"contact-email\">Email</label>
                    </div>
                  </div>
                  <div class=\"col-sm-12\">
                    <div class=\"form-wrap\">
                      <div class=\"textarea-lined-wrap\">
                        <textarea class=\"form-input\" id=\"contact-message\" name=\"message\" data-constraints=\"@Required\"></textarea>
                        <label class=\"form-label\" for=\"contact-message\">Message</label>
                      </div>
                    </div>
                  </div>
                  <div class=\"col-sm-8\">
                    <button class=\"btn btn-primary btn-block\" type=\"submit\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\">Send</button>
                  </div>

                </div>
              </form>
            </div>
            <div class=\"col-lg-5 col-xl-4\">
              <div class=\"inset-lg-right-15 inset-xl-right-0\">
                <div class=\"row row-30\">
                  <div class=\"col-md-10 col-lg-12\">
                    <h3 class=\"text-spacing--25\">How to Find Us</h3>
                    <p class=\"text-black\">
                      If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit Shelter at one of our comfortable offices.
                      
                    </p>
                  </div>
                  <div class=\"col-md-6 col-lg-12\">
                    <h5 class=\"text-spacing--25\">Headquarters</h5>
                    <address class=\"contact-info\">
                      <p class=\"text-uppercase text-black\">9863 - 9867 MILL ROAD, CAMBRIDGE, MG09 99HT.</p>
                      <dl class=\"list-terms-inline\">
                        <dt>Telephone</dt>
                        <dd><a class=\"link-black\" href=\"tel:#\">+1 800 603 6035</a></dd>
                      </dl>
                      <dl class=\"list-terms-inline\">
                        <dt>E-mail</dt>
                        <dd><a class=\"link-primary\" href=\"mailto:#\">mail@demolink.org</a></dd>
                      </dl>
                    </address>
                  </div>
                  <div class=\"col-md-6 col-lg-12\">
                    <h5 class=\"text-spacing--25\">Support Centre</h5>
                    <address class=\"contact-info\">
                      <p class=\"text-uppercase text-black\">9870 ST VINCENT PLACE, GLASGOW, DC 45 FR 45</p>
                      <dl class=\"list-terms-inline\">
                        <dt>Telephone</dt>
                        <dd><a class=\"link-black\" href=\"tel:#\">+1 800 603 6035</a></dd>
                      </dl>
                      <dl class=\"list-terms-inline\">
                        <dt>E-mail</dt>
                        <dd><a class=\"link-primary\" href=\"mailto:#\">mail@demolink.org</a></dd>
                      </dl>
                    </address>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!--<div class=\"rd-google-map rd-google-map__model\" data-zoom=\"15\" data-x=\"-73.9874068\" data-y=\"40.643180\" data-styles=\"\">-->
      
        <div class=\"map\" style=\"background: #202020;fill: #202020;\">
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59062.88602778264!2d114.14537287033113!3d22.299557506605215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3403e2eda332980f%3A0xf08ab3badbeac97c!2z0JPQvtC90LrQvtC90LM!5e0!3m2!1sru!2slv!4v1533207772040\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
\t    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/contact-us.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 32,  75 => 25,  68 => 23,  62 => 21,  54 => 19,  52 => 18,  49 => 17,  45 => 16,  37 => 10,  35 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url({{ 'bg-image-8.jpg'|media }});\">
        <div class=\"container\">
          <div class=\"page-title text-center\">
            <h2>Contact Us</h2>
          </div>
        </div>
</section>

{% if this.page[\"breadscrumbs\"] %}
<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    {%for item,url in this.page[\"breadscrumbs\"]%}
                    <li>
                        {% if url!=\"\" %}
                            <a href=\"{{url}}\">{{item}}</a>
                        {% else %}
                            {{item}}
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
{% endif %}

      <section class=\"section-60 section-md-top-90 section-md-bottom-100\">
        <div class=\"container\">
          <div class=\"row justify-content-lg-between row-60\">
            <div class=\"col-lg-7 col-xl-6\">
              <h3 class=\"text-spacing--25\">Get in Touch</h3>
              <form class=\"rd-mailform form-modern\" id=\"contact-us-form\" data-form-output=\"form-output-global\" data-form-type=\"contact\" method=\"post\" action=\"bat/rd-mailform.php\">
                <div class=\"row row-30\">
                  <div class=\"col-md-6\">
                    <div class=\"form-wrap\">
                      <input class=\"form-input\" id=\"contact-name\" type=\"text\" name=\"name\" data-constraints=\"@Required\">
                      <label class=\"form-label\" for=\"contact-name\">Name</label>
                    </div>
                  </div>
                  <div class=\"col-md-6\">
                    <div class=\"form-wrap\">
                      <input class=\"form-input\" id=\"contact-email\" type=\"email\" name=\"email\" data-constraints=\"@Email @Required\">
                      <label class=\"form-label\" for=\"contact-email\">Email</label>
                    </div>
                  </div>
                  <div class=\"col-sm-12\">
                    <div class=\"form-wrap\">
                      <div class=\"textarea-lined-wrap\">
                        <textarea class=\"form-input\" id=\"contact-message\" name=\"message\" data-constraints=\"@Required\"></textarea>
                        <label class=\"form-label\" for=\"contact-message\">Message</label>
                      </div>
                    </div>
                  </div>
                  <div class=\"col-sm-8\">
                    <button class=\"btn btn-primary btn-block\" type=\"submit\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\">Send</button>
                  </div>

                </div>
              </form>
            </div>
            <div class=\"col-lg-5 col-xl-4\">
              <div class=\"inset-lg-right-15 inset-xl-right-0\">
                <div class=\"row row-30\">
                  <div class=\"col-md-10 col-lg-12\">
                    <h3 class=\"text-spacing--25\">How to Find Us</h3>
                    <p class=\"text-black\">
                      If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit Shelter at one of our comfortable offices.
                      
                    </p>
                  </div>
                  <div class=\"col-md-6 col-lg-12\">
                    <h5 class=\"text-spacing--25\">Headquarters</h5>
                    <address class=\"contact-info\">
                      <p class=\"text-uppercase text-black\">9863 - 9867 MILL ROAD, CAMBRIDGE, MG09 99HT.</p>
                      <dl class=\"list-terms-inline\">
                        <dt>Telephone</dt>
                        <dd><a class=\"link-black\" href=\"tel:#\">+1 800 603 6035</a></dd>
                      </dl>
                      <dl class=\"list-terms-inline\">
                        <dt>E-mail</dt>
                        <dd><a class=\"link-primary\" href=\"mailto:#\">mail@demolink.org</a></dd>
                      </dl>
                    </address>
                  </div>
                  <div class=\"col-md-6 col-lg-12\">
                    <h5 class=\"text-spacing--25\">Support Centre</h5>
                    <address class=\"contact-info\">
                      <p class=\"text-uppercase text-black\">9870 ST VINCENT PLACE, GLASGOW, DC 45 FR 45</p>
                      <dl class=\"list-terms-inline\">
                        <dt>Telephone</dt>
                        <dd><a class=\"link-black\" href=\"tel:#\">+1 800 603 6035</a></dd>
                      </dl>
                      <dl class=\"list-terms-inline\">
                        <dt>E-mail</dt>
                        <dd><a class=\"link-primary\" href=\"mailto:#\">mail@demolink.org</a></dd>
                      </dl>
                    </address>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!--<div class=\"rd-google-map rd-google-map__model\" data-zoom=\"15\" data-x=\"-73.9874068\" data-y=\"40.643180\" data-styles=\"\">-->
      
        <div class=\"map\" style=\"background: #202020;fill: #202020;\">
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59062.88602778264!2d114.14537287033113!3d22.299557506605215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3403e2eda332980f%3A0xf08ab3badbeac97c!2z0JPQvtC90LrQvtC90LM!5e0!3m2!1sru!2slv!4v1533207772040\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
\t    </div>", "/var/www/html/passport/themes/passport/pages/contact-us.htm", "");
    }
}
