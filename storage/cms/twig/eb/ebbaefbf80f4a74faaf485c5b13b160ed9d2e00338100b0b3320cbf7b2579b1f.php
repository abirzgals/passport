<?php

/* /var/www/html/passport/themes/passport/pages/about-us.htm */
class __TwigTemplate_f2b1db1d2608a7116e969ad99f60451b8181910004d67ad25bfb540f4dfe3f1d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url(";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-6.jpg");
        echo ");\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>About us</h2>
      </div>
    </div>
</section>

";
        // line 9
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["breadscrumbs"] ?? null) : null)) {
            // line 10
            echo "<section class=\"bg-default bg-athens-gray\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["breadscrumbs"] ?? null) : null));
            foreach ($context['_seq'] as $context["item"] => $context["url"]) {
                // line 17
                echo "                    <li>
                        ";
                // line 18
                if (($context["url"] != "")) {
                    // line 19
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $context["url"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 21
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "
                        ";
                }
                // line 23
                echo "                    </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['item'], $context['url'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
";
        }
        // line 32
        echo "
<section class=\"section-66 bg-athens-gray\">
        <div class=\"container\">
          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h3>You Can Trust Us</h3>
            </div>
          </div>
          <div class=\"row justify-content-xl-between row-40\">
            <div class=\"col-md-6 col-xl-5 text-secondary\">
              <div class=\"inset-md-right-15 inset-xl-right-0\">
                <p>Our agency can provide you and your family with quality insurance for your personal, business and financial needs. Our knowledgeable agents can help your find affordable and reliable car insurance, home insurance, commercial insurance or life insurance.</p>
                <p>
                  Our focus is to help our clients grow their assets. We believe in being local and we strive to improve the communities where we live and work.
                  
                </p>
              </div>
            </div>
            <div class=\"col-md-6\">
              <div class=\"shift-sm-top-1\">
                
                <img src=\"";
        // line 53
        echo $this->extensions['System\Twig\Extension']->mediaFilter("home-slider-1-slide-1.jpg");
        echo "\">
                <!--
                <ul class=\"list-progress\">
                  <li>
                    <p class=\"animated fadeIn\">Life Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-red-orange-1\" data-value=\"70\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Car Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-dodger-blue\" data-value=\"54\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Business Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-gorse\" data-value=\"87\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Travel Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-primary\" data-value=\"90\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                </ul>
                -->
                
              </div>
            </div>
          </div>
        </div>
</section>

      <!--
      <section class=\"section-60 section-md-100 bg-gray-dark bg-image overlay-5\" style=\"background-image: url(";
        // line 82
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-6.jpg");
        echo ");\">
        <div class=\"container\">
          <div class=\"row row-40\">
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"";
        // line 87
        echo $this->extensions['System\Twig\Extension']->mediaFilter("icon-01.png");
        echo "\" width=\"53\" height=\"50\" alt=\"\"></figure>
                <div class=\"text-large counter\">1450</div>
                <h5 class=\"box-header text-white\">Happy Clients</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"";
        // line 94
        echo $this->extensions['System\Twig\Extension']->mediaFilter("icon-02.png");
        echo "\" width=\"41\" height=\"43\" alt=\"\"></figure>
                <div class=\"text-large counter\">23</div>
                <h5 class=\"box-header\">Insurance Products</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"";
        // line 101
        echo $this->extensions['System\Twig\Extension']->mediaFilter("icon-03.png");
        echo "\" width=\"52\" height=\"53\" alt=\"\"></figure>
                <div class=\"text-large counter\">10</div>
                <h5 class=\"box-header\">Years of Experience</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"";
        // line 108
        echo $this->extensions['System\Twig\Extension']->mediaFilter("icon-04.png");
        echo "\" width=\"55\" height=\"46\" alt=\"\"></figure>
                <div class=\"text-large counter\">196</div>
                <h5 class=\"box-header\">Professional Agents</h5>
              </div>
            </div>
          </div>
        </div>
      </section> style=\"background-size:50%;background-repeat:no-repeat;background-position: left;background-image: url(";
        // line 115
        echo $this->extensions['System\Twig\Extension']->mediaFilter("gallery-4-1200x797_original.jpg");
        echo ");\"
    -->





    <section class=\"section-60 section-md-90  bg-image bg-image-sm-hide\">
        <div class=\"container\">
          <div class=\"row justify-content-md-end\">
          
          
            <div class=\"col-md-6\">
              <div class=\"shift-sm-top-1\"> 
                <img src=\"";
        // line 129
        echo $this->extensions['System\Twig\Extension']->mediaFilter("post-1-870x412.jpg");
        echo "\">
              </div>
            </div>
          
            <div class=\"col-md-6 col-lg-6 col-xl-6\">
              <h3 class=\"text-spacing--25 inset-md-left-100\">Why Choose Us</h3>
              <div class=\"inset-md-left-30 inset-md-right-30\">
                <ul class=\"list-xl\">
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-check51\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">Our Mission</a></h5>
                          <p class=\"text-black-05\">Our mission is to provide outstanding service and superior coverage to every one of our clients.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-chat75\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">24/7 Support</a></h5>
                          <p class=\"text-black-05\">Our dedicated support professionals are always here to help no matter your issue.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-briefcase50\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">Our Commitment</a></h5>
                          <p class=\"text-black-05\">We are proud to offer our clients competitive pricing, a broad choice of products and unparalleled advocacy.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
</section>

<!--
      <section class=\"section-66 section-md-bottom-90 bg-whisper\">
        <div class=\"container\">
          <div class=\"row row-40\">
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"";
        // line 187
        echo $this->extensions['System\Twig\Extension']->mediaFilter("team-9-246x300.jpg");
        echo "\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">Amanda Smith</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Chief Executive Officer</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"";
        // line 204
        echo $this->extensions['System\Twig\Extension']->mediaFilter("team-10-246x300.jpg");
        echo "\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">John Doe</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Executive Vice President</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"";
        // line 221
        echo $this->extensions['System\Twig\Extension']->mediaFilter("team-11-246x300.jpg");
        echo "\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">Vanessa Ives</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Chief Financial Officer</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"";
        // line 238
        echo $this->extensions['System\Twig\Extension']->mediaFilter("team-12-246x300.jpg");
        echo "\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">David Nicholson</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Vice President of Claims</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
      -->
      
      
      <section class=\"section-40 bg-image bg-gray-dark overlay-8\" style=\"background-image: url(";
        // line 259
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-9.jpg");
        echo ");\">
        <div class=\"container text-center\">
          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h3 class=\"text-spacing--25\">What Clients Say</h3>
            </div>
          </div>
          <div class=\"row justify-content-sm-center justify-content-md-start row-60\">
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>Tom Brown was able to save us a substantial amount of premium dollars and improve our insurance coverage..</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"";
        // line 280
        echo $this->extensions['System\Twig\Extension']->mediaFilter("clients-testimonials-7-113x113.png");
        echo "\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Alex Murphy</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>I have found working with the associates a genuine pleasure. The evaluation of our needs is well-researched..</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"";
        // line 300
        echo $this->extensions['System\Twig\Extension']->mediaFilter("clients-testimonials-8-113x113.png");
        echo "\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Amelia Condon</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>I have used many different insurance agencies and this agency is by far the best. Thank you a lot, keep up going this way.</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"";
        // line 320
        echo $this->extensions['System\Twig\Extension']->mediaFilter("clients-testimonials-9-113x113.png");
        echo "\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Jack Smith</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
          </div>
        </div>
      </section>
      
      
      
 ";
        // line 333
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("contact-form"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/about-us.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 333,  419 => 320,  396 => 300,  373 => 280,  349 => 259,  325 => 238,  305 => 221,  285 => 204,  265 => 187,  204 => 129,  187 => 115,  177 => 108,  167 => 101,  157 => 94,  147 => 87,  139 => 82,  107 => 53,  84 => 32,  75 => 25,  68 => 23,  62 => 21,  54 => 19,  52 => 18,  49 => 17,  45 => 16,  37 => 10,  35 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url({{ 'bg-image-6.jpg'|media }});\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>About us</h2>
      </div>
    </div>
</section>

{% if this.page[\"breadscrumbs\"] %}
<section class=\"bg-default bg-athens-gray\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    {%for item,url in this.page[\"breadscrumbs\"]%}
                    <li>
                        {% if url!=\"\" %}
                            <a href=\"{{url}}\">{{item}}</a>
                        {% else %}
                            {{item}}
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
{% endif %}

<section class=\"section-66 bg-athens-gray\">
        <div class=\"container\">
          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h3>You Can Trust Us</h3>
            </div>
          </div>
          <div class=\"row justify-content-xl-between row-40\">
            <div class=\"col-md-6 col-xl-5 text-secondary\">
              <div class=\"inset-md-right-15 inset-xl-right-0\">
                <p>Our agency can provide you and your family with quality insurance for your personal, business and financial needs. Our knowledgeable agents can help your find affordable and reliable car insurance, home insurance, commercial insurance or life insurance.</p>
                <p>
                  Our focus is to help our clients grow their assets. We believe in being local and we strive to improve the communities where we live and work.
                  
                </p>
              </div>
            </div>
            <div class=\"col-md-6\">
              <div class=\"shift-sm-top-1\">
                
                <img src=\"{{ 'home-slider-1-slide-1.jpg'|media }}\">
                <!--
                <ul class=\"list-progress\">
                  <li>
                    <p class=\"animated fadeIn\">Life Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-red-orange-1\" data-value=\"70\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Car Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-dodger-blue\" data-value=\"54\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Business Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-gorse\" data-value=\"87\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                  <li>
                    <p class=\"animated fadeIn\">Travel Insurance</p>
                    <div class=\"progress-bar-js progress-bar-horizontal progress-bar-primary\" data-value=\"90\" data-stroke=\"4\" data-easing=\"linear\" data-counter=\"true\" data-duration=\"1000\" data-trail=\"100\"></div>
                  </li>
                </ul>
                -->
                
              </div>
            </div>
          </div>
        </div>
</section>

      <!--
      <section class=\"section-60 section-md-100 bg-gray-dark bg-image overlay-5\" style=\"background-image: url({{ 'bg-image-6.jpg'|media }});\">
        <div class=\"container\">
          <div class=\"row row-40\">
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"{{ 'icon-01.png'|media }}\" width=\"53\" height=\"50\" alt=\"\"></figure>
                <div class=\"text-large counter\">1450</div>
                <h5 class=\"box-header text-white\">Happy Clients</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"{{ 'icon-02.png'|media }}\" width=\"41\" height=\"43\" alt=\"\"></figure>
                <div class=\"text-large counter\">23</div>
                <h5 class=\"box-header\">Insurance Products</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"{{ 'icon-03.png'|media }}\" width=\"52\" height=\"53\" alt=\"\"></figure>
                <div class=\"text-large counter\">10</div>
                <h5 class=\"box-header\">Years of Experience</h5>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-3\">
              <div class=\"box-counter box-counter-inverse box-counter-white\">
                <figure><img src=\"{{ 'icon-04.png'|media }}\" width=\"55\" height=\"46\" alt=\"\"></figure>
                <div class=\"text-large counter\">196</div>
                <h5 class=\"box-header\">Professional Agents</h5>
              </div>
            </div>
          </div>
        </div>
      </section> style=\"background-size:50%;background-repeat:no-repeat;background-position: left;background-image: url({{ 'gallery-4-1200x797_original.jpg'|media }});\"
    -->





    <section class=\"section-60 section-md-90  bg-image bg-image-sm-hide\">
        <div class=\"container\">
          <div class=\"row justify-content-md-end\">
          
          
            <div class=\"col-md-6\">
              <div class=\"shift-sm-top-1\"> 
                <img src=\"{{ 'post-1-870x412.jpg'|media }}\">
              </div>
            </div>
          
            <div class=\"col-md-6 col-lg-6 col-xl-6\">
              <h3 class=\"text-spacing--25 inset-md-left-100\">Why Choose Us</h3>
              <div class=\"inset-md-left-30 inset-md-right-30\">
                <ul class=\"list-xl\">
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-check51\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">Our Mission</a></h5>
                          <p class=\"text-black-05\">Our mission is to provide outstanding service and superior coverage to every one of our clients.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-chat75\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">24/7 Support</a></h5>
                          <p class=\"text-black-05\">Our dedicated support professionals are always here to help no matter your issue.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                  <li>
                    <article class=\"icon-box-horizontal\">
                      <div class=\"unit unit-horizontal unit-spacing-md\">
                        <div class=\"unit-left\"><span class=\"icon icon-primary icon-md material-design-briefcase50\"></span></div>
                        <div class=\"unit-body\">
                          <h5 class=\"prefix-lg-right--10\"><a href=\"#\">Our Commitment</a></h5>
                          <p class=\"text-black-05\">We are proud to offer our clients competitive pricing, a broad choice of products and unparalleled advocacy.</p>
                        </div>
                      </div>
                    </article>

                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
</section>

<!--
      <section class=\"section-66 section-md-bottom-90 bg-whisper\">
        <div class=\"container\">
          <div class=\"row row-40\">
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"{{ 'team-9-246x300.jpg'|media }}\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">Amanda Smith</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Chief Executive Officer</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"{{ 'team-10-246x300.jpg'|media }}\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">John Doe</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Executive Vice President</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"{{ 'team-11-246x300.jpg'|media }}\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">Vanessa Ives</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Chief Financial Officer</p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"col-sm-6 col-md-4 col-lg-3\">
              <div class=\"thumbnail-variant-2-wrap\">
                <div class=\"thumbnail thumbnail-variant-2\">
                  <figure class=\"thumbnail-image\"><img src=\"{{ 'team-12-246x300.jpg'|media }}\" alt=\"\" width=\"246\" height=\"300\"/>
                  </figure>
                  <div class=\"thumbnail-inner\">
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary material-icons-local_phone\"></span><a class=\"link-white\" href=\"tel:#\">+1 (409) 987–5874</a></div>
                    <div class=\"link-group\"><span class=\"icon icon-xxs icon-primary fa-envelope-o\"></span><a class=\"link-white\" href=\"mailto:#\">info@demolink.org</a></div>
                  </div>
                  <div class=\"thumbnail-caption\">
                    <p class=\"text-header\"><a href=\"team-member-profile.html\">David Nicholson</a></p>
                    <div class=\"divider divider-md\"></div>
                    <p class=\"text-caption\">Vice President of Claims</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
      -->
      
      
      <section class=\"section-40 bg-image bg-gray-dark overlay-8\" style=\"background-image: url({{ 'bg-image-9.jpg'|media }});\">
        <div class=\"container text-center\">
          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h3 class=\"text-spacing--25\">What Clients Say</h3>
            </div>
          </div>
          <div class=\"row justify-content-sm-center justify-content-md-start row-60\">
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>Tom Brown was able to save us a substantial amount of premium dollars and improve our insurance coverage..</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"{{ 'clients-testimonials-7-113x113.png'|media }}\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Alex Murphy</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>I have found working with the associates a genuine pleasure. The evaluation of our needs is well-researched..</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"{{ 'clients-testimonials-8-113x113.png'|media }}\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Amelia Condon</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
            <div class=\"col-md-4\">
                    <blockquote class=\"quote-vertical quote-vertical-inverse\">
                      <div class=\"quote-body\">
                        <div class=\"quote-open\">
                          <svg version=\"1.1\" baseprofile=\"tiny\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"16px\" height=\"12px\" viewbox=\"0 0 21 15\" overflow=\"scroll\" xml:space=\"preserve\" preserveAspectRatio=\"none\">
                            <path d=\"M9.597,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.349,1.316                  c-1.287,0-2.414-0.44-3.382-1.316C0.482,12.811,0,11.758,0,10.535c0-1.226,0.58-2.716,1.739-4.473L5.603,0H9.34L6.956,6.37                  C8.716,7.145,9.597,8.493,9.597,10.412z M20.987,10.412c0,1.306-0.473,2.399-1.418,3.277c-0.944,0.876-2.06,1.316-3.35,1.316                  c-1.288,0-2.415-0.44-3.381-1.316c-0.966-0.879-1.45-1.931-1.45-3.154c0-1.226,0.582-2.716,1.74-4.473L16.994,0h3.734l-2.382,6.37                  C20.106,7.145,20.987,8.493,20.987,10.412z\"></path>
                          </svg>
                        </div>
                        <p class=\"quote-text\">
                          <q>I have used many different insurance agencies and this agency is by far the best. Thank you a lot, keep up going this way.</q>
                        </p>
                      </div>
                      <div class=\"quote-meta\">
                        <figure class=\"quote-image\"><img src=\"{{ 'clients-testimonials-9-113x113.png'|media }}\" alt=\"\" width=\"113\" height=\"113\"/>
                        </figure>
                        <cite>Jack Smith</cite>
                        <p class=\"caption\">Client</p>
                      </div>
                    </blockquote>
            </div>
          </div>
        </div>
      </section>
      
      
      
 {% partial 'contact-form' %}", "/var/www/html/passport/themes/passport/pages/about-us.htm", "");
    }
}
