<?php

/* /var/www/html/passport/themes/passport/pages/terms/terms.htm */
class __TwigTemplate_42de7d73e2403fa6225867aaa52f692143e22769675e2ec4c51c8838835b137e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5 privacy\" style=\"background-image: url(";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-4.jpg");
        echo "\">
        <div class=\"container\">
          <div class=\"page-title text-center\">
            <h2>Terms of use</h2>
          </div>
        </div>
      </section>
      
";
        // line 9
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["breadscrumbs"] ?? null) : null)) {
            // line 10
            echo "<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["breadscrumbs"] ?? null) : null));
            foreach ($context['_seq'] as $context["item"] => $context["url"]) {
                // line 17
                echo "                    <li>
                        ";
                // line 18
                if (($context["url"] != "")) {
                    // line 19
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $context["url"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 21
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "
                        ";
                }
                // line 23
                echo "                    </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['item'], $context['url'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
";
        }
        // line 32
        echo "
<section class=\"section-xl-bottom-120 privacy\">
        <div class=\"container\">

          <div class=\"row justify-content-md-center row-30\">
            <div class=\"col-xl-10\">
             
             
             ";
        // line 40
        echo $this->extensions['Cms\Twig\Extension']->contentFunction("terms.htm");
        echo "
             
             
             
            </div>
            <div class=\"col-xl-10\"><a class=\"link-primary\" href=\"mailto:#\">privacy@demolink.org</a></div>
          </div>
        </div>
      </section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/terms/terms.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 40,  84 => 32,  75 => 25,  68 => 23,  62 => 21,  54 => 19,  52 => 18,  49 => 17,  45 => 16,  37 => 10,  35 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5 privacy\" style=\"background-image: url({{ 'bg-image-4.jpg'|media }}\">
        <div class=\"container\">
          <div class=\"page-title text-center\">
            <h2>Terms of use</h2>
          </div>
        </div>
      </section>
      
{% if this.page[\"breadscrumbs\"] %}
<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    {%for item,url in this.page[\"breadscrumbs\"]%}
                    <li>
                        {% if url!=\"\" %}
                            <a href=\"{{url}}\">{{item}}</a>
                        {% else %}
                            {{item}}
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
{% endif %}

<section class=\"section-xl-bottom-120 privacy\">
        <div class=\"container\">

          <div class=\"row justify-content-md-center row-30\">
            <div class=\"col-xl-10\">
             
             
             {{ content('terms.htm') }}
             
             
             
            </div>
            <div class=\"col-xl-10\"><a class=\"link-primary\" href=\"mailto:#\">privacy@demolink.org</a></div>
          </div>
        </div>
      </section>", "/var/www/html/passport/themes/passport/pages/terms/terms.htm", "");
    }
}
