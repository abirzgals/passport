<?php

/* /var/www/html/passport/themes/passport/pages/page-404.htm */
class __TwigTemplate_ee66d72800bb0a12598efca8e1d8c5b210e7bd71f8a6ea0458464028b3ca029e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section section-single novi-background bg-gray-darker bg-image\" style=\"background-image: url(images/bg-404.jpg);\">
        <div class=\"section-single-inner\">
          <header class=\"section-single-header\">
            <div class=\"divider-primary\"></div>
            <div class=\"page-head-inner\">
              <div class=\"container text-center\"><a class=\"brand brand-md brand-inverse\" href=\"index.html\"><img src=\"images/brand-varinat-1.png\" width=\"118\" height=\"34\" alt=\"\"></a></div>
            </div>
          </header>
          <div class=\"section-single-main\">
            <div class=\"container\">
              <div class=\"row justify-content-md-center\">
                <div class=\"col-md-9 col-lg-8\">
                  <h5>Sorry, but page was not found</h5>
                  <div class=\"text-extra-large-bordered\">
                    <p>404</p>
                  </div>
                  <p class=\"text-white\">You may have mistyped the address or the page may have moved.</p>
                  <div class=\"group-xl\"><a class=\"btn btn-primary\" href=\"index.html\">Back to home</a><a class=\"btn btn-white-outline\" href=\"contacts-us.html\">contact us</a></div>
                </div>
              </div>
            </div>
          </div>

          <section class=\"section-single-footer\">
            <div class=\"page-foot-inner\">
              <div class=\"container text-center\">
                <div class=\"row\">
                  <div class=\"col-sm-12\">
                    <p class=\"rights\"><span>&nbsp;&#169;&nbsp;</span><span id=\"copyright-year\"></span><span>Shelter</span><a class=\"link-primary-inverse\" href=\"privacy.html\">Privacy Policy</a></p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"divider-primary\"></div>
          </section>

        </div>
      </section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/page-404.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section section-single novi-background bg-gray-darker bg-image\" style=\"background-image: url(images/bg-404.jpg);\">
        <div class=\"section-single-inner\">
          <header class=\"section-single-header\">
            <div class=\"divider-primary\"></div>
            <div class=\"page-head-inner\">
              <div class=\"container text-center\"><a class=\"brand brand-md brand-inverse\" href=\"index.html\"><img src=\"images/brand-varinat-1.png\" width=\"118\" height=\"34\" alt=\"\"></a></div>
            </div>
          </header>
          <div class=\"section-single-main\">
            <div class=\"container\">
              <div class=\"row justify-content-md-center\">
                <div class=\"col-md-9 col-lg-8\">
                  <h5>Sorry, but page was not found</h5>
                  <div class=\"text-extra-large-bordered\">
                    <p>404</p>
                  </div>
                  <p class=\"text-white\">You may have mistyped the address or the page may have moved.</p>
                  <div class=\"group-xl\"><a class=\"btn btn-primary\" href=\"index.html\">Back to home</a><a class=\"btn btn-white-outline\" href=\"contacts-us.html\">contact us</a></div>
                </div>
              </div>
            </div>
          </div>

          <section class=\"section-single-footer\">
            <div class=\"page-foot-inner\">
              <div class=\"container text-center\">
                <div class=\"row\">
                  <div class=\"col-sm-12\">
                    <p class=\"rights\"><span>&nbsp;&#169;&nbsp;</span><span id=\"copyright-year\"></span><span>Shelter</span><a class=\"link-primary-inverse\" href=\"privacy.html\">Privacy Policy</a></p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"divider-primary\"></div>
          </section>

        </div>
      </section>", "/var/www/html/passport/themes/passport/pages/page-404.htm", "");
    }
}
