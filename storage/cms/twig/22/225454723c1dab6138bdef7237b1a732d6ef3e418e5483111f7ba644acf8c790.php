<?php

/* /var/www/html/passport/themes/passport/partials/footer.htm */
class __TwigTemplate_ad213c6666b9cbcf4fe67b5acc7b1d447f7d32bda1dcf77a9af07a51daf86400 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"page-foot bg-cello\">
        <div class=\"container\">
          <hr class=\"divider-bismark-04\">
        </div>
        <section class=\"section-35\">
          <div class=\"container text-center\">
            <div class=\"row flex-row-md-reverse justify-content-md-between align-items-md-center row-15\">
              <!--<div class=\"col-md-6 text-md-right\">
                <div class=\"group-sm group-middle\">
                  <p class=\"font-italic text-white\">Follow Us:</p>
                  <ul class=\"list-inline list-inline-reset\">
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-facebook\" href=\"#\"></a></li>
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-twitter\" href=\"#\"></a></li>
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-google-plus\" href=\"#\"></a></li>
                  </ul>
                </div>
              </div>-->
              <div class=\"col-md-12 \">
                <p class=\"rights text-white\"><span class=\"copyright-year\"></span><span>&nbsp;&#169;&nbsp;</span><span>Shelter.&nbsp;</span>
                <ul class=\"list-marked-variant-3\">
                        <li><a href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("terms/privacy-policy");
        echo "\">Privacy Policy</a></li>
                        <li><a href=\"";
        // line 22
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("terms/cookies");
        echo "\">Cookie Policy</a></li>
                        <li><a href=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("terms/terms");
        echo "\">Terms of use</a></li>
                      </ul>
              </div>
            </div>
          </div>
        </section>
      </footer>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 23,  49 => 22,  45 => 21,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer class=\"page-foot bg-cello\">
        <div class=\"container\">
          <hr class=\"divider-bismark-04\">
        </div>
        <section class=\"section-35\">
          <div class=\"container text-center\">
            <div class=\"row flex-row-md-reverse justify-content-md-between align-items-md-center row-15\">
              <!--<div class=\"col-md-6 text-md-right\">
                <div class=\"group-sm group-middle\">
                  <p class=\"font-italic text-white\">Follow Us:</p>
                  <ul class=\"list-inline list-inline-reset\">
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-facebook\" href=\"#\"></a></li>
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-twitter\" href=\"#\"></a></li>
                    <li><a class=\"icon icon-circle icon-blue-bayoux icon-xxs-smallest fa fa-google-plus\" href=\"#\"></a></li>
                  </ul>
                </div>
              </div>-->
              <div class=\"col-md-12 \">
                <p class=\"rights text-white\"><span class=\"copyright-year\"></span><span>&nbsp;&#169;&nbsp;</span><span>Shelter.&nbsp;</span>
                <ul class=\"list-marked-variant-3\">
                        <li><a href=\"{{\"terms/privacy-policy\"|page}}\">Privacy Policy</a></li>
                        <li><a href=\"{{\"terms/cookies\"|page}}\">Cookie Policy</a></li>
                        <li><a href=\"{{\"terms/terms\"|page}}\">Terms of use</a></li>
                      </ul>
              </div>
            </div>
          </div>
        </section>
      </footer>", "/var/www/html/passport/themes/passport/partials/footer.htm", "");
    }
}
