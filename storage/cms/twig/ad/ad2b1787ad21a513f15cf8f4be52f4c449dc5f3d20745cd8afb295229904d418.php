<?php

/* /var/www/html/passport/themes/passport/partials/pswp.htm */
class __TwigTemplate_2e12be12739b533829e0ad5b43ec7bdecc5f3fbef70ea68cfa0d86b8f7c4c7a1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"snackbars\" id=\"form-output-global\"></div>
    <div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
      <div class=\"pswp__bg\"></div>
      <div class=\"pswp__scroll-wrap\">
        <div class=\"pswp__container\">
          <div class=\"pswp__item\"></div>
          <div class=\"pswp__item\"></div>
          <div class=\"pswp__item\"></div>
        </div>
        <div class=\"pswp__ui pswp__ui--hidden\">
          <div class=\"pswp__top-bar\">
            <div class=\"pswp__counter\"></div>
            <button class=\"pswp__button pswp__button--close\" title=\"Close (Esc)\"></button>
            <button class=\"pswp__button pswp__button--share\" title=\"Share\"></button>
            <button class=\"pswp__button pswp__button--fs\" title=\"Toggle fullscreen\"></button>
            <button class=\"pswp__button pswp__button--zoom\" title=\"Zoom in/out\"></button>
            <div class=\"pswp__preloader\">
              <div class=\"pswp__preloader__icn\">
                <div class=\"pswp__preloader__cut\">
                  <div class=\"pswp__preloader__donut\"></div>
                </div>
              </div>
            </div>
          </div>
          <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
            <div class=\"pswp__share-tooltip\"></div>
          </div>
          <button class=\"pswp__button pswp__button--arrow--left\" title=\"Previous (arrow left)\"></button>
          <button class=\"pswp__button pswp__button--arrow--right\" title=\"Next (arrow right)\"></button>
          <div class=\"pswp__caption\">
            <div class=\"pswp__caption__cent\"></div>
          </div>
        </div>
      </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/partials/pswp.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"snackbars\" id=\"form-output-global\"></div>
    <div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
      <div class=\"pswp__bg\"></div>
      <div class=\"pswp__scroll-wrap\">
        <div class=\"pswp__container\">
          <div class=\"pswp__item\"></div>
          <div class=\"pswp__item\"></div>
          <div class=\"pswp__item\"></div>
        </div>
        <div class=\"pswp__ui pswp__ui--hidden\">
          <div class=\"pswp__top-bar\">
            <div class=\"pswp__counter\"></div>
            <button class=\"pswp__button pswp__button--close\" title=\"Close (Esc)\"></button>
            <button class=\"pswp__button pswp__button--share\" title=\"Share\"></button>
            <button class=\"pswp__button pswp__button--fs\" title=\"Toggle fullscreen\"></button>
            <button class=\"pswp__button pswp__button--zoom\" title=\"Zoom in/out\"></button>
            <div class=\"pswp__preloader\">
              <div class=\"pswp__preloader__icn\">
                <div class=\"pswp__preloader__cut\">
                  <div class=\"pswp__preloader__donut\"></div>
                </div>
              </div>
            </div>
          </div>
          <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
            <div class=\"pswp__share-tooltip\"></div>
          </div>
          <button class=\"pswp__button pswp__button--arrow--left\" title=\"Previous (arrow left)\"></button>
          <button class=\"pswp__button pswp__button--arrow--right\" title=\"Next (arrow right)\"></button>
          <div class=\"pswp__caption\">
            <div class=\"pswp__caption__cent\"></div>
          </div>
        </div>
      </div>
    </div>", "/var/www/html/passport/themes/passport/partials/pswp.htm", "");
    }
}
