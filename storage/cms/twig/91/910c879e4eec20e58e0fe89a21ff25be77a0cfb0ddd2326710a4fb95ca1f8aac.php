<?php

/* /var/www/html/passport/themes/passport/layouts/index.htm */
class __TwigTemplate_8803edbad38c9f43c3648e972f13be14c5503e61de5963efc16f3de60122cf31 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"wide wow-animation\" lang=\"en\">
  <head>
    <base href=\"http://34.220.166.135/passport/\">
    <title>Home</title>
    <meta name=\"format-detection\" content=\"telephone=no\">
    <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta charset=\"utf-8\">
    <link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Arvo:400,400i,700,700i%7CLato:300,300italic,400,400italic,700,900%7CPlayfair+Display:700italic,900\">
    
    <!--<link rel=\"stylesheet\" href=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.css");
        echo "\">-->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/fonts.css");
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\">


\t\t<!--[if lt IE 10]>
    <div style=\"background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;\"><a href=\"http://windows.microsoft.com/en-US/internet-explorer/\"><img src=\"";
        // line 20
        echo $this->extensions['System\Twig\Extension']->mediaFilter("ie8-panel/warning_bar_0000_us.jpg");
        echo "\" border=\"0\" height=\"42\" width=\"820\" alt=\"You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.\"></a></div>
    <script src=\"js/html5shiv.min.js\"></script>
\t\t<![endif]-->
  </head>
  <body>
    <div class=\"page\">
      <!--<div class=\"page-loader page-loader-variant-1\">
        <div><a class=\"brand brand-md brand-inverse\" href=\"index.html\"><img src=\"";
        // line 27
        echo $this->extensions['System\Twig\Extension']->mediaFilter("brand-varinat-2.png");
        echo "\" width=\"118\" height=\"34\" alt=\"\"></a>
          <div class=\"page-loader-body\">
            <div id=\"spinningSquaresG\">
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_1\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_2\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_3\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_4\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_5\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_6\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_7\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_8\"></div>
            </div>
          </div>
        </div>
      </div>-->
      ";
        // line 42
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("page-head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 43
        echo "      ";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 44
        echo "      ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer-selection"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 45
        echo "      ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 46
        echo "      
      
<!-- Modal -->
<div class=\"modal fade\" id=\"dialog-thankyou\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\"><span class=\"icon icon-xs icon-primary material-icons-comment\"></span>Thank&nbsp;you,&nbsp;your&nbsp;message&nbsp;has&nbsp;been&nbsp;sent.</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\" style=\"text-align:center;\">
         We&nbsp;will&nbsp;contact&nbsp;you&nbsp;soon.
      </div>
      <!--<div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
      </div>-->
    </div>
  </div>
</div>
      
      
    </div>
    ";
        // line 70
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("pswp"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 71
        echo "

 <script  src=\"https://code.jquery.com/jquery-3.2.1.min.js\"  integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"  crossorigin=\"anonymous\"></script>
    <script src=\"";
        // line 74
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/core.min.js");
        echo "\"></script>
    ";
        // line 75
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        echo "    
    <script src=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/script.js");
        echo "\"></script>
    <script src=\"";
        // line 77
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/cookie-policy.js");
        echo "\"></script>
  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/layouts/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 77,  146 => 76,  137 => 75,  133 => 74,  128 => 71,  124 => 70,  98 => 46,  93 => 45,  88 => 44,  85 => 43,  81 => 42,  63 => 27,  53 => 20,  46 => 16,  42 => 15,  37 => 13,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html class=\"wide wow-animation\" lang=\"en\">
  <head>
    <base href=\"http://34.220.166.135/passport/\">
    <title>Home</title>
    <meta name=\"format-detection\" content=\"telephone=no\">
    <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta charset=\"utf-8\">
    <link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Arvo:400,400i,700,700i%7CLato:300,300italic,400,400italic,700,900%7CPlayfair+Display:700italic,900\">
    
    <!--<link rel=\"stylesheet\" href=\"{{ 'assets/css/bootstrap.css'|theme }}\">-->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"{{ 'assets/css/fonts.css'|theme }}\">
    <link rel=\"stylesheet\" href=\"{{ 'assets/css/style.css'|theme }}\">


\t\t<!--[if lt IE 10]>
    <div style=\"background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;\"><a href=\"http://windows.microsoft.com/en-US/internet-explorer/\"><img src=\"{{ 'ie8-panel/warning_bar_0000_us.jpg'|media }}\" border=\"0\" height=\"42\" width=\"820\" alt=\"You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.\"></a></div>
    <script src=\"js/html5shiv.min.js\"></script>
\t\t<![endif]-->
  </head>
  <body>
    <div class=\"page\">
      <!--<div class=\"page-loader page-loader-variant-1\">
        <div><a class=\"brand brand-md brand-inverse\" href=\"index.html\"><img src=\"{{ 'brand-varinat-2.png'|media }}\" width=\"118\" height=\"34\" alt=\"\"></a>
          <div class=\"page-loader-body\">
            <div id=\"spinningSquaresG\">
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_1\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_2\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_3\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_4\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_5\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_6\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_7\"></div>
              <div class=\"spinningSquaresG\" id=\"spinningSquaresG_8\"></div>
            </div>
          </div>
        </div>
      </div>-->
      {% partial \"page-head\" %}
      {% page %}
      {% partial 'footer-selection' %}
      {% partial 'footer' %}
      
      
<!-- Modal -->
<div class=\"modal fade\" id=\"dialog-thankyou\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\"><span class=\"icon icon-xs icon-primary material-icons-comment\"></span>Thank&nbsp;you,&nbsp;your&nbsp;message&nbsp;has&nbsp;been&nbsp;sent.</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\" style=\"text-align:center;\">
         We&nbsp;will&nbsp;contact&nbsp;you&nbsp;soon.
      </div>
      <!--<div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
      </div>-->
    </div>
  </div>
</div>
      
      
    </div>
    {% partial 'pswp' %}


 <script  src=\"https://code.jquery.com/jquery-3.2.1.min.js\"  integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"  crossorigin=\"anonymous\"></script>
    <script src=\"{{ 'assets/js/core.min.js'|theme }}\"></script>
    {% framework extras %}    
    <script src=\"{{ 'assets/js/script.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/cookie-policy.js'|theme }}\"></script>
  </body>
</html>", "/var/www/html/passport/themes/passport/layouts/index.htm", "");
    }
}
