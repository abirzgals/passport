<?php

/* /var/www/html/passport/themes/passport/pages/second-passport.htm */
class __TwigTemplate_08f82b415f5d5c2c1780df395809902b04960f6fe288001f90e83ddf199500b8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url(";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("bg-image-4.jpg");
        echo ");\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>";
        // line 4
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "category", array())), "html", null, true);
        echo "</h2>
      </div>
    </div>
</section>



";
        // line 11
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["breadscrumbs"] ?? null) : null)) {
            // line 12
            echo "<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array())) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["breadscrumbs"] ?? null) : null));
            foreach ($context['_seq'] as $context["item"] => $context["url"]) {
                // line 19
                echo "                    <li>
                        ";
                // line 20
                if (($context["url"] != "")) {
                    // line 21
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $context["url"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</a>
                        ";
                } else {
                    // line 23
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "
                        ";
                }
                // line 25
                echo "                    </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['item'], $context['url'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
";
        }
        // line 34
        echo "              
                  
<section class=\"bg-decoration-wrap section-xl-top-30 bg-decoration-bottom bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\" style=\"text-align:center;\">
                  <ul class=\"product-subcategory\">
                  ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["submenu"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 42
            echo "                      <li><a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\"  class=\"";
            if ((twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "slug", array()) == twig_get_attribute($this->env, $this->source, $context["item"], "slug", array()))) {
                echo "active";
            }
            echo "\" >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></li>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                  </ul>
            </div>
        </div>
    </div>
</section>

<section class=\"bg-decoration-wrap bg-decoration-bottom section-xl-top-40 bg-default\">
    <div class=\"container bg-decoration-content\">
      <div class=\"row justify-content-md-center\">
        <div class=\"col-lg-10 col-xl-12\">
        
          <div class=\"row row-40\" style=\"display: -webkit-box;\">
          <div class=\"col-xl-12\">
                <h2>
                    ";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "name", array()), "html", null, true);
        echo "
                </h2>
          </div>
          </div>
          
          <div class=\"row row-40\" style=\"display: -webkit-box;\">
    
            <div class=\"col-md-12 col-xl-8\">
                   ";
        // line 66
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "top_description", array());
        echo "
            </div>
            <div class=\"col-md-6 col-xl-4\">
              <div class=\"pricing-table\">
                <div class=\"pricing-table-body\">
                  <h5 class=\"pricing-table-header\">";
        // line 71
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "name", array()), "html", null, true);
        echo "</h5>
                  <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\"> ";
        // line 72
        echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "price", array()), 0, "floor"), "html", null, true);
        echo "</span><span class=\"small small-bottom\">  </span></div>
                  <ul class=\"pricing-list\">
                     ";
        // line 74
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "benefits", array());
        echo "
                  </ul>
                </div>
                <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
              </div>
            </div>

          </div>
          
          
         <div class=\"row row-40\" style=\"display: -webkit-box;\">
             <div class=\"col-md-12 col-xl-12\">
                      ";
        // line 86
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "mid_description", array());
        echo "            
    \t\t </div>
    \t </div>  
    \t 
        <div class=\"row align-items-md-end row-40\" style=\"display: -webkit-box;\">
         <div class=\"col-md-12 col-xl-12\">
            <div class=\"map\">
    \t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d487523.31609237107!2d-";
        // line 93
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "longitude", array());
        echo "!3d";
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "tatitude", array());
        echo "!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c0d6eb96db8d1c1%3A0x276a3788e18b7994!2z0JDQvdGC0LjQs9GD0LAg0Lgg0JHQsNGA0LHRg9C00LA!5e0!3m2!1sru!2slv!4v1533119099178\" width=\"100%\" height=\"300px\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"  allowfullscreen></iframe>
    \t    </div>
    \t  </div>
    \t</div>              
          
         <div class=\"row row-40\" style=\"display: -webkit-box;\">
             <div class=\"col-md-12 col-xl-12\">
                      ";
        // line 100
        echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "bottom_description", array());
        echo "            
    \t\t </div>
    \t </div>  
          
          
        </div>
      </div>
      <hr style=\"margin-top: 30px;border: 1px solid #000;\" >
    </div>
    <!--<div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>-->
</section>


<!--
      <section class=\"section-60 section-md-100 bg-image bg-accent\">
        <div class=\"container\">
          <div class=\"row justify-content-lg-center row-30\">
            <div class=\"col-lg-8 col-xl-7\">
              <h3>Not Sure Which Plan is Right For You?</h3>
              <p class=\"text-white\" style=\"max-width: 430px;\">If you are in doubt of which plan to opt for, subscribe to our newsletter and we will try to help you make the right decision.</p>
            </div>
            <div class=\"col-lg-4 col-xl-3\"><a class=\"btn btn-xl btn-xl-bigger btn-white-outline\" href=\"contacts-us.html\">Subscribe Now</a></div>
          </div>
        </div>
      </section>
      -->
 ";
        // line 126
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("contact-form"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/pages/second-passport.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 126,  198 => 100,  186 => 93,  176 => 86,  161 => 74,  156 => 72,  152 => 71,  144 => 66,  133 => 58,  117 => 44,  102 => 42,  98 => 41,  89 => 34,  80 => 27,  73 => 25,  67 => 23,  59 => 21,  57 => 20,  54 => 19,  50 => 18,  42 => 12,  40 => 11,  30 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-30 section-md-40 section-lg-120 bg-gray-dark page-title-wrap overlay-5\" style=\"background-image: url({{ 'bg-image-4.jpg'|media }});\">
    <div class=\"container\">
      <div class=\"page-title text-center\">
        <h2>{{record.category|title }}</h2>
      </div>
    </div>
</section>



{% if this.page[\"breadscrumbs\"] %}
<section class=\"bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\">
            
                <ul class='breadscrumbs'>
                    {%for item,url in this.page[\"breadscrumbs\"]%}
                    <li>
                        {% if url!=\"\" %}
                            <a href=\"{{url}}\">{{item}}</a>
                        {% else %}
                            {{item}}
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
    
            </div>    
        </div>    
    </div>    
</section>
{% endif %}
              
                  
<section class=\"bg-decoration-wrap section-xl-top-30 bg-decoration-bottom bg-default\">
    <div class=\"container\">
        <div class=\"row justify-content-md-center\">
            <div class=\"col-md-12 col-xl-12\" style=\"text-align:center;\">
                  <ul class=\"product-subcategory\">
                  {%for item in submenu%}
                      <li><a href=\"{{'citizenship'|page({'slug':item.slug})}}\"  class=\"{% if record.slug==item.slug%}active{% endif %}\" >{{item.name}}</a></li>
                  {%endfor%}
                  </ul>
            </div>
        </div>
    </div>
</section>

<section class=\"bg-decoration-wrap bg-decoration-bottom section-xl-top-40 bg-default\">
    <div class=\"container bg-decoration-content\">
      <div class=\"row justify-content-md-center\">
        <div class=\"col-lg-10 col-xl-12\">
        
          <div class=\"row row-40\" style=\"display: -webkit-box;\">
          <div class=\"col-xl-12\">
                <h2>
                    {{record.name }}
                </h2>
          </div>
          </div>
          
          <div class=\"row row-40\" style=\"display: -webkit-box;\">
    
            <div class=\"col-md-12 col-xl-8\">
                   {{record.top_description|raw }}
            </div>
            <div class=\"col-md-6 col-xl-4\">
              <div class=\"pricing-table\">
                <div class=\"pricing-table-body\">
                  <h5 class=\"pricing-table-header\">{{record.name}}</h5>
                  <div class=\"pricing-object pricing-object-lg\"><span class=\"small small-top\">\$</span><span class=\"price\"> {{record.price|round(0, 'floor') }}</span><span class=\"small small-bottom\">  </span></div>
                  <ul class=\"pricing-list\">
                     {{record.benefits|raw }}
                  </ul>
                </div>
                <div class=\"pricing-table-footer\"><a class=\"btn btn-primary btn-block\" href=\"register.html\">Sign up</a></div>
              </div>
            </div>

          </div>
          
          
         <div class=\"row row-40\" style=\"display: -webkit-box;\">
             <div class=\"col-md-12 col-xl-12\">
                      {{record.mid_description|raw }}            
    \t\t </div>
    \t </div>  
    \t 
        <div class=\"row align-items-md-end row-40\" style=\"display: -webkit-box;\">
         <div class=\"col-md-12 col-xl-12\">
            <div class=\"map\">
    \t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d487523.31609237107!2d-{{record.longitude|raw}}!3d{{record.tatitude|raw}}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c0d6eb96db8d1c1%3A0x276a3788e18b7994!2z0JDQvdGC0LjQs9GD0LAg0Lgg0JHQsNGA0LHRg9C00LA!5e0!3m2!1sru!2slv!4v1533119099178\" width=\"100%\" height=\"300px\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"  allowfullscreen></iframe>
    \t    </div>
    \t  </div>
    \t</div>              
          
         <div class=\"row row-40\" style=\"display: -webkit-box;\">
             <div class=\"col-md-12 col-xl-12\">
                      {{record.bottom_description|raw }}            
    \t\t </div>
    \t </div>  
          
          
        </div>
      </div>
      <hr style=\"margin-top: 30px;border: 1px solid #000;\" >
    </div>
    <!--<div class=\"bg-decoration-object bg-iron bg-iron-custom\"></div>-->
</section>


<!--
      <section class=\"section-60 section-md-100 bg-image bg-accent\">
        <div class=\"container\">
          <div class=\"row justify-content-lg-center row-30\">
            <div class=\"col-lg-8 col-xl-7\">
              <h3>Not Sure Which Plan is Right For You?</h3>
              <p class=\"text-white\" style=\"max-width: 430px;\">If you are in doubt of which plan to opt for, subscribe to our newsletter and we will try to help you make the right decision.</p>
            </div>
            <div class=\"col-lg-4 col-xl-3\"><a class=\"btn btn-xl btn-xl-bigger btn-white-outline\" href=\"contacts-us.html\">Subscribe Now</a></div>
          </div>
        </div>
      </section>
      -->
 {% partial 'contact-form' %}", "/var/www/html/passport/themes/passport/pages/second-passport.htm", "");
    }
}
