<?php

/* /var/www/html/passport/themes/passport/partials/page-head.htm */
class __TwigTemplate_9fc0ab856306163fe61279bca7101ebc3a3f39d97fc9b685ea41163fcd974936 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"page-head\">
        <div class=\"rd-navbar-wrap\">
          <nav class=\"rd-navbar rd-navbar-corporate-light\" data-layout=\"rd-navbar-fixed\" data-sm-layout=\"rd-navbar-fixed\" data-md-device-layout=\"rd-navbar-fixed\" data-md-layout=\"rd-navbar-fixed\" data-lg-layout=\"rd-navbar-static\" data-xl-layout=\"rd-navbar-static\" data-xxl-layout=\"rd-navbar-static\" data-lg-device-layout=\"rd-navbar-static\" data-xl-device-layout=\"rd-navbar-static\" data-xxl-device-layout=\"rd-navbar-static\" data-stick-up-clone=\"false\" data-md-stick-up-offset=\"53px\" data-lg-stick-up-offset=\"53px\" data-md-stick-up=\"true\" data-lg-stick-up=\"true\">
            <div class=\"rd-navbar-inner\">
              <div class=\"rd-navbar-aside-wrap\">
                <div class=\"rd-navbar-aside\">
                  <div class=\"rd-navbar-aside-toggle\" data-rd-navbar-toggle=\".rd-navbar-aside\"><span></span></div>
                  <div class=\"rd-navbar-aside-content\">
                    <ul class=\"rd-navbar-aside-group list-units\">
                      <li>
                        <div class=\"unit flex-row unit-spacing-xs align-items-center\">
                          <div class=\"unit-left\"><span class=\"icon icon-xxs icon-cello material-icons-phone\"></span></div>
                          <div class=\"unit-body\"><a class=\"link-secondary\" href=\"tel:#\">+1 (888) 123–4567</a></div>
                        </div>
                      </li>
                      <li>
                        <div class=\"unit flex-row unit-spacing-xs align-items-center\">
                          <div class=\"unit-left\"><span class=\"icon icon-xxs-small icon-cello fa-envelope-o\"></span></div>
                          <div class=\"unit-body\"><a class=\"link-secondary\" href=\"mailto:#\">info@citizenship.com</a></div>
                        </div>
                      </li>
                    </ul>
                    <div class=\"rd-navbar-aside-group\">

                    </div>
                  </div>
                </div>
                <!--<div class=\"rd-navbar-search\">
                  <form class=\"rd-search\" action=\"https://www.google.lv/search?q=site:www.gov.uk+get+citizenship\" method=\"GET\" data-search-live=\"rd-search-results-live\" data-search-live-count=\"6\">
                    <div class=\"rd-search-inner\">
                      <div class=\"form-wrap\">
                        <label class=\"form-label\" for=\"rd-search-form-input\">Search...</label>
                        <input class=\"form-input\" id=\"rd-search-form-input\" type=\"text\" name=\"q\" autocomplete=\"off\">
                      </div>
                      <button class=\"rd-search-submit\" type=\"submit\"></button>
                    </div>
                    <div class=\"rd-search-results-live\" id=\"rd-search-results-live\"></div>
                  </form>
                  <button class=\"rd-navbar-search-toggle\" data-rd-navbar-toggle=\".rd-navbar-search, .rd-navbar-search-wrap\"></button>
                </div>-->
              </div>
              <div class=\"rd-navbar-group\">
                <div class=\"rd-navbar-panel\">
                  <button class=\"rd-navbar-toggle\" data-rd-navbar-toggle=\".rd-navbar-nav-wrap\"><span></span></button><a class=\"rd-navbar-brand brand\" href=\"";
        // line 44
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("brand.png");
        echo "\" width=\"118\" height=\"34\" alt=\"\"></a>
                </div>
                <div class=\"rd-navbar-nav-wrap\">
                  <div class=\"rd-navbar-nav-inner\">
                    <ul class=\"rd-navbar-nav\">
                      <li class=\"";
        // line 49
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "index")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">Home</a>
                      </li>
                      <li class=\"";
        // line 51
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "about-us")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about-us");
        echo "\">About Us</a>
                      </li>

                      
                      <li class=\"";
        // line 55
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "citizenships") || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "citizenship"))) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenships");
        echo "\" id=\"citizenship-link\">Citizenship</a>
                        <ul class=\"rd-navbar-dropdown\">
                        
                        
                          ";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["citizenship_menu"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 60
            echo "                              <li><a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("citizenship", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></li>
                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                        </ul>
                      </li>
                      <li class=\"";
        // line 64
        if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "second-passports") || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "second-passport"))) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passports");
        echo "\">Second passport</a>
                        <ul class=\"rd-navbar-dropdown\">
                          ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["second_passport_menu"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 67
            echo "                              <li><a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("second-passport", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></li>
                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                        </ul>
                      </li>
                      <li class=\"";
        // line 71
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "contact-us")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact-us");
        echo "\">Contacts</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/passport/themes/passport/partials/page-head.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 71,  152 => 69,  141 => 67,  137 => 66,  128 => 64,  124 => 62,  113 => 60,  109 => 59,  98 => 55,  87 => 51,  78 => 49,  68 => 44,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"page-head\">
        <div class=\"rd-navbar-wrap\">
          <nav class=\"rd-navbar rd-navbar-corporate-light\" data-layout=\"rd-navbar-fixed\" data-sm-layout=\"rd-navbar-fixed\" data-md-device-layout=\"rd-navbar-fixed\" data-md-layout=\"rd-navbar-fixed\" data-lg-layout=\"rd-navbar-static\" data-xl-layout=\"rd-navbar-static\" data-xxl-layout=\"rd-navbar-static\" data-lg-device-layout=\"rd-navbar-static\" data-xl-device-layout=\"rd-navbar-static\" data-xxl-device-layout=\"rd-navbar-static\" data-stick-up-clone=\"false\" data-md-stick-up-offset=\"53px\" data-lg-stick-up-offset=\"53px\" data-md-stick-up=\"true\" data-lg-stick-up=\"true\">
            <div class=\"rd-navbar-inner\">
              <div class=\"rd-navbar-aside-wrap\">
                <div class=\"rd-navbar-aside\">
                  <div class=\"rd-navbar-aside-toggle\" data-rd-navbar-toggle=\".rd-navbar-aside\"><span></span></div>
                  <div class=\"rd-navbar-aside-content\">
                    <ul class=\"rd-navbar-aside-group list-units\">
                      <li>
                        <div class=\"unit flex-row unit-spacing-xs align-items-center\">
                          <div class=\"unit-left\"><span class=\"icon icon-xxs icon-cello material-icons-phone\"></span></div>
                          <div class=\"unit-body\"><a class=\"link-secondary\" href=\"tel:#\">+1 (888) 123–4567</a></div>
                        </div>
                      </li>
                      <li>
                        <div class=\"unit flex-row unit-spacing-xs align-items-center\">
                          <div class=\"unit-left\"><span class=\"icon icon-xxs-small icon-cello fa-envelope-o\"></span></div>
                          <div class=\"unit-body\"><a class=\"link-secondary\" href=\"mailto:#\">info@citizenship.com</a></div>
                        </div>
                      </li>
                    </ul>
                    <div class=\"rd-navbar-aside-group\">

                    </div>
                  </div>
                </div>
                <!--<div class=\"rd-navbar-search\">
                  <form class=\"rd-search\" action=\"https://www.google.lv/search?q=site:www.gov.uk+get+citizenship\" method=\"GET\" data-search-live=\"rd-search-results-live\" data-search-live-count=\"6\">
                    <div class=\"rd-search-inner\">
                      <div class=\"form-wrap\">
                        <label class=\"form-label\" for=\"rd-search-form-input\">Search...</label>
                        <input class=\"form-input\" id=\"rd-search-form-input\" type=\"text\" name=\"q\" autocomplete=\"off\">
                      </div>
                      <button class=\"rd-search-submit\" type=\"submit\"></button>
                    </div>
                    <div class=\"rd-search-results-live\" id=\"rd-search-results-live\"></div>
                  </form>
                  <button class=\"rd-navbar-search-toggle\" data-rd-navbar-toggle=\".rd-navbar-search, .rd-navbar-search-wrap\"></button>
                </div>-->
              </div>
              <div class=\"rd-navbar-group\">
                <div class=\"rd-navbar-panel\">
                  <button class=\"rd-navbar-toggle\" data-rd-navbar-toggle=\".rd-navbar-nav-wrap\"><span></span></button><a class=\"rd-navbar-brand brand\" href=\"{{ 'index'|page }}\"><img src=\"{{ 'brand.png'|media}}\" width=\"118\" height=\"34\" alt=\"\"></a>
                </div>
                <div class=\"rd-navbar-nav-wrap\">
                  <div class=\"rd-navbar-nav-inner\">
                    <ul class=\"rd-navbar-nav\">
                      <li class=\"{% if this.page.id=='index'%}active{% endif %}\"><a href=\"{{ 'index'|page }}\">Home</a>
                      </li>
                      <li class=\"{% if this.page.id=='about-us'%}active{% endif %}\"><a href=\"{{ 'about-us'|page }}\">About Us</a>
                      </li>

                      
                      <li class=\"{% if this.page.id=='citizenships' or this.page.id=='citizenship'%}active{% endif %}\"><a href=\"{{ 'citizenships'|page }}\" id=\"citizenship-link\">Citizenship</a>
                        <ul class=\"rd-navbar-dropdown\">
                        
                        
                          {%for item in citizenship_menu%}
                              <li><a href=\"{{'citizenship'|page({'slug':item.slug})}}\">{{item.name}}</a></li>
                          {%endfor%}
                        </ul>
                      </li>
                      <li class=\"{% if this.page.id=='second-passports' or this.page.id=='second-passport'%}active{% endif %}\"><a href=\"{{ 'second-passports'|page }}\">Second passport</a>
                        <ul class=\"rd-navbar-dropdown\">
                          {%for item in second_passport_menu%}
                              <li><a href=\"{{'second-passport'|page({'slug':item.slug})}}\">{{item.name}}</a></li>
                          {%endfor%}
                        </ul>
                      </li>
                      <li class=\"{% if this.page.id=='contact-us'%}active{% endif %}\"><a href=\"{{ 'contact-us'|page }}\">Contacts</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>", "/var/www/html/passport/themes/passport/partials/page-head.htm", "");
    }
}
