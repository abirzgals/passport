<?php 
class Cms5b72d048b5caf009134728_7d18f4352ae96faaa25cb8a6e3856038Class extends Cms\Classes\LayoutCode
{
public function onStart()
{
    $this["second_passport_menu"] = Db::table('passport_offers_products')->where('category',"second passport")->where('disabled','<>','1')->get();
    $this["citizenship_menu"] = Db::table('passport_offers_products')->where('category',"citizenship")->where('disabled','<>','1')->get();
    
    if (!Request::secure()) {
        //return Redirect::to('https://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
    }
    $pid = Input::get("pid");
   if ($pid){
        setcookie("pid", $pid);
    }
    
    $this->page['breadscrumbs'] = array(
        'Home'=>$this->page['site_link'].'/passport',
        $this->page->title=>"",
    );    
    
    $ipaddress = '';
    
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress .= getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress .= getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress .= getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress .= getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress .= getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
        
    $this->page["ipaddress"] = $ipaddress;
    
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
    $this->page["actual_link"] = $actual_link;
    $this->page["site_link"] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER["HTTP_HOST"];
    
    $companyTimezone = 'Europe/Riga';
    $current = new DateTime("now", new DateTimeZone($companyTimezone) );
    $current_formatted = $current->format('H');
    
    if ($current_formatted>=7 and $current_formatted<=17)
     $this->page["contact_widget"] = "phone";
    else
     $this->page["contact_widget"] = "email";
     
    $user = BackendAuth::getUser();
    if ($user) {
        $this->page["user"] = $user;
    }  
    
    
}
public function onEnd() {

}
public function onDoCurl($url, $fields)
{
    $fields_string = "";
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');    
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    // Set so curl_exec returns the result instead of outputting it.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // Get the response and close the channel.
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
    //sample
    //return '{"error":false,"text":"Thank you, your message has been sent. We will contact you soon.","redirect":"","redirectDelay":5,"resultId":217,"gid":"24xqz832fl5xmv5qf3tv50h1tykqrs2t"}';
}
public function onSendUniversalForm()
{
    
    $data = post();

    if (isset($_COOKIE['pid']))
        $pid = $_COOKIE['pid'];
    else $pid="";

    $rules = [
        'firstname' => 'required',
        'email' => 'required|email',
        'subject' => 'required',
        'message' => 'required'
    ];

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}


    $validation = Validator::make($data, $rules);

    if ($validation->fails()) {
        Log::warning("form submission validation failed");
        throw new ValidationException($validation);
    }
    $this['result'] = DB::table('passport_offers_contactform')->insert(
        [
            'firstname' => input('firstname'),
            'lastname' => input('lastname'),
            'email' => input('email'),
            'skype' => input('skype'),
            'phone' => input('phone'),
            'message' => input('message'),
            
            'product_type' => input('product_type'),
            'product_name' => input('product_name'),
            'promocode' => input('promocode'),
            
            'email_subscribtion' => input('LEAD_UF_CRM_1528724976'),
            
            'created_at' => date('Y-m-d H:i:s'),
            'ip' =>  $ip,
            'url' => input('url'),
            'pid' => $pid
        ]
    );

 
    $numbers = "1510331652847";
    $sec = "mtpy57";
    $form_id="14";
    $url = 'https://bitrix.offshorelicense.com/pub/form.php?view=frame&form_id='.$form_id.'&widget_user_lang=en&sec='.$sec.'&r='.$numbers;

    $fields = array(
    	'LEAD_NAME' => input('firstname'),
    	'LEAD_LAST_NAME' =>input('lastname'),
    	'LEAD_EMAIL' => input('email'),
    	'LEAD_PHONE' => input('phone'),
    	'LEAD_IM' => input('skype'),
    	'LEAD_COMMENTS' => input('message'),
    	'LEAD_SOURCE_DESCRIPTION' => input('url'),
    	'LEAD_UF_CRM_1472455700[0]' => input('LEAD_UF_CRM_1472455700_0'),
    	'LEAD_UF_CRM_1472455700[1]' => input('LEAD_UF_CRM_1472455700_1'),
    	'LEAD_UF_CRM_1477656743' => input('product_type'),
    	'LEAD_UF_CRM_1515071921' => input('product_name'),
    	'LEAD_UF_CRM_1528724976' => input('LEAD_UF_CRM_1528724976'),
    	'LEAD_UF_CRM_1530705157' => $pid,
    	'from' => urlencode(input('url')),
    	'sessid' => urlencode(session_id())
    );
    
    
    $this["response"] = $this->onDoCurl($url, $fields);
    //$this["response"]='{"error":false,"text":"Thank you, your message has been sent. We will contact you soon.","redirect":"","redirectDelay":5,"resultId":217,"gid":"24xqz832fl5xmv5qf3tv50h1tykqrs2t"}';
    $encodedResult = json_decode( $this["response"]);

    if ($encodedResult->error=="true"){
        Log::warning("form submission error: curl failed");
        Flash::error('Error');
    } else {
        //Flash::success('Thank you, your message has been sent. We will contact you soon.');
    }
    Log::info("form submission completed: ".$encodedResult->text);
    $this['result'] = $encodedResult->text;
    

}
}
