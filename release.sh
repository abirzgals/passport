#!/usr/bin/env bash
/bin/echo -e "\e[1;32m\nProcessing: git pull\n\e[0m"
sudo git pull
/bin/echo -e "\e[1;32m\nProcessing: chmod\n\e[0m"
sudo chmod -R 777 plugins
sudo chmod -R 777 storage
sudo chmod -R 777 vendor
sudo chmod -R 777 themes
#/bin/echo -e "\e[1;32m\nProcessing: queue worker reread\n\e[0m"
#sudo supervisorctl reread
#/bin/echo -e "\e[1;32m\nProcessing: queue worker update\n\e[0m"
#sudo supervisorctl update
#/bin/echo -e "\e[1;32m\nProcessing: queue starting worker\n\e[0m"
#sudo supervisorctl start october-worker:*

