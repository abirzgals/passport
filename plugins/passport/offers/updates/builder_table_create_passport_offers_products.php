<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePassportOffersProducts extends Migration
{
    public function up()
    {
        Schema::create('passport_offers_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('category')->nullable();
            $table->string('name')->nullable();
            $table->decimal('price', 22, 2)->nullable();
            $table->text('top_description')->nullable();
            $table->text('bottom_description')->nullable();
            $table->text('benefits')->nullable();
            $table->decimal('lat', 9, 6)->nullable();
            $table->decimal('lon', 9, 6)->nullable();
            $table->boolean('hidden')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('passport_offers_products');
    }
}
