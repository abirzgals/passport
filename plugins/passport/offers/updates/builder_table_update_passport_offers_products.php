<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePassportOffersProducts extends Migration
{
    public function up()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
