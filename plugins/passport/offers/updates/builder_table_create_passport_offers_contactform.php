<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePassportOffersContactform extends Migration
{
    public function up()
    {
        Schema::create('passport_offers_contactform', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('phone')->nullable();
            $table->string('skype')->nullable();
            $table->text('message')->nullable();
            $table->string('ip')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('email')->nullable();
            $table->string('url')->nullable();
            $table->string('product_type')->nullable();
            $table->string('product_name')->nullable();
            $table->string('promocode')->nullable();
            $table->string('email_subscribtion')->nullable();
            $table->string('pid')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('passport_offers_contactform');
    }
}
