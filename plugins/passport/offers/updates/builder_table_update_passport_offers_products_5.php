<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePassportOffersProducts5 extends Migration
{
    public function up()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->text('mid_description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->dropColumn('mid_description');
        });
    }
}
