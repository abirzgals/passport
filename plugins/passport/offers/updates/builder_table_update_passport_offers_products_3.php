<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePassportOffersProducts3 extends Migration
{
    public function up()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->string('disabled')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->boolean('disabled')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
