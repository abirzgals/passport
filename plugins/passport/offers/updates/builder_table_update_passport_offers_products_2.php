<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePassportOffersProducts2 extends Migration
{
    public function up()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->renameColumn('hidden', 'disabled');
        });
    }
    
    public function down()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->renameColumn('disabled', 'hidden');
        });
    }
}
