<?php namespace Passport\Offers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePassportOffersProducts4 extends Migration
{
    public function up()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->integer('order_id')->nullable();
            $table->string('featured')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('passport_offers_products', function($table)
        {
            $table->dropColumn('order_id');
            $table->dropColumn('featured');
        });
    }
}
