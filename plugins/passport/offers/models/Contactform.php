<?php namespace Passport\Offers\Models;

use Model;

/**
 * Model
 */
class Contactform extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'passport_offers_contactform';
}
