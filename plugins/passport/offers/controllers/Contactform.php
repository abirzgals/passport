<?php namespace Passport\Offers\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Contactform extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Passport.Offers', 'main-menu-item2');
    }
}
